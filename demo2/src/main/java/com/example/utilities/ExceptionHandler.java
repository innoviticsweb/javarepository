package com.example.utilities;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/*import org.slf4j.Logger;
import org.slf4j.LoggerFactory;*/
import org.springframework.stereotype.Service;

import com.example.exceptions.BusinessException;
import com.example.exceptions.GeneralException;
import com.example.exceptions.IntegrationException;
import com.example.service.AbstractService;

@Service
public class ExceptionHandler {

	protected static final Logger logger = LogManager.getLogger(AbstractService.class);

	//protected static final Logger logger = LoggerFactory.getLogger(ExceptionHandler.class);

	public BusinessException handleAsBusinessException(Exception exception,ErrorCode errorCode)
	  
	{
		  	logger.error("Exception Caught");
			logger.error("Exception Cause:"+exception.getCause());
			logger.error("Exception Message:"+exception.getMessage());
			logger.error("Exception Details:",exception);
			BusinessException businessException=new  BusinessException(errorCode.getCode(),DateUtility.getCurrentDate(),errorCode.getMessage(), exception.getMessage(), exception.getStackTrace());
			
			logger.error("Exxception:::"+businessException.toString());
			
			return businessException;
	  }
	public BusinessException handleIntegrationExceptionAsBusinessException(IntegrationException integrationException,ErrorCode errorCode)
	  
	{
			logger.info("Handling Exception as an Integration Exceptionin the Exception Handler::::");
		  	logger.error("Exception Caught");
			logger.error("Exception Cause:"+integrationException.getCause());
			logger.error("Exception Message:"+integrationException.getMessage());
			logger.error("Exception Details:",integrationException.toString());
			BusinessException businessException=new  BusinessException(errorCode.getCode(),DateUtility.getCurrentDate(),errorCode.getMessage(), integrationException.getMessage(), integrationException.getStackTrace());
			
			logger.error("Exxception:::"+businessException.toString());
			
			return businessException;
	  }
	public IntegrationException handleAsIntegrationException(Exception exception,ErrorCode errorCode)
	  
	{
		  	logger.error("Exception Caught");
			logger.error("Exception Cause:"+exception.getCause());
			logger.error("Exception Message:"+exception.getMessage());
			logger.error("Exception Details:",exception);
			IntegrationException integrationException=new   IntegrationException(errorCode.getCode(),DateUtility.getCurrentDate(),errorCode.getMessage(), exception.getMessage(), exception.getStackTrace());
			
			logger.error("Exxception:::"+integrationException.toString());
			
			return integrationException;	  
		}
}
