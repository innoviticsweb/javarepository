package com.example.utilities;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class ArrayUtility {

	public boolean isArrayPopulated(Object[] array) 
	{
		return (array!=null&&array.length>0);
	}

	public List<Object> generateObjectListFromObjectArray(Object[] array)
	{
		List<Object> list=new ArrayList<Object>();
		
		if(this.isArrayPopulated(array))
		{
			for (int i=0;i<array.length;i++)
			{
				list.add(array[i]);
			}
		}
		return list;
	}
	
}
