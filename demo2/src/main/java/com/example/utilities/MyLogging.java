package com.example.utilities;



import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.example.properties.ConfigProperties;


public final class MyLogging {	

    	private final static ConfigProperties configProperties = new ConfigProperties();
	 	static MyFormatter formatter = null;
	    static FileHandler fileHandler = null;
	    static ConsoleHandler consoleHandler = null;
	    static String preName = "Demo-"+DateUtility.getCurrentYearMonthDay();
	    static Long time = System.currentTimeMillis();
	    //static String file_path = "e:\\Development\\Java_Projects\\SystemLogs\\new\\" + preName +".log";
	    static String file_path = configProperties.getPath()+ preName +".log";
	    //static String file_path =ENV.getProperty("log.file.path");
	    // setup method
	    static public void setup() throws SecurityException, IOException {
	        /*if (fileHandler == null)*/
	            fileHandler = new FileHandler(file_path, true);
	        if (consoleHandler == null)
	            consoleHandler = new ConsoleHandler();
	        if (formatter == null)
	            formatter = new MyFormatter();	        	
	    }

	    static public Logger classLogger(String className) {
	        Logger logger = Logger.getLogger(className);
	        logger.setUseParentHandlers(false);
	        /*if (fileHandler == null || consoleHandler == null || formatter == null)*/    
	            try {
	                setup();
	                fileHandler.setFormatter(formatter);

	                consoleHandler.setFormatter(formatter);
	                Handler[] handlers = logger.getHandlers();

	                // array of registered handlers
	                for (Handler handler : handlers) {
	                   logger.removeHandler(handler);
	                }

	                logger.setLevel(Level.INFO);
	                logger.addHandler(fileHandler);
	            } catch (SecurityException | IOException e) {
	                e.printStackTrace();
	            }

	        return logger;
	    }

	    static public Logger classLogger(String className, String log_prefix) {
	        preName = log_prefix;
	        file_path = "e:\\Development\\Java_Projects\\SystemLogs\\new\\" + preName+ ".log";
	        return classLogger(className);
	    }
	}