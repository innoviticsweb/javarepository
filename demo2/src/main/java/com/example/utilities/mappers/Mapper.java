package com.example.utilities.mappers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.example.business.BaseBusinessEntity;
import com.example.business.BusinessSearchCriteria;
import com.example.db.BaseEntity;
import com.example.utilities.PaginatedEntity;

@Component
public abstract class Mapper <S extends BaseEntity, T extends BaseBusinessEntity>
{
	

	protected static final Logger logger = LoggerFactory.getLogger(Mapper.class);

	protected  abstract S convertBusinessUnitToBasicUnit(T t , boolean save);
	
	protected  abstract T convertBasicUnitToBusinessUnit(S s);
	
	
	public List<S> convertBusinessListToBasicList(List<T> businessEntities, boolean save) {
		List<S> entities = new ArrayList<S>();

		if (checkIfBusinessBasicListIsPopulated(businessEntities)) {
			for (T businessEntity : businessEntities) {
				entities.add(convertBusinessUnitToBasicUnit(businessEntity, save));
			}
		}

		return entities;
	}

	public List<T> convertBasicListToBusinessList(List<S> baseEntities) {
		List<T> businessEntities = new ArrayList<T>();

		if (checkIfBasicListIsPopulated(baseEntities)) {
			for (S baseEntity : baseEntities) {
				businessEntities.add(convertBasicUnitToBusinessUnit(baseEntity));
			}
		}

		return businessEntities;
	}

	public boolean checkIfBasicListIsPopulated(List<S> entities) {
		return (entities != null && !entities.isEmpty());
	}

	public boolean checkIfBusinessBasicListIsPopulated(List<T> businessEntities) {
		return (businessEntities != null && !businessEntities.isEmpty());
	}


	public PaginatedEntity<T> convertBasicPageToBusinessPage(Page<S> baseEntities,BusinessSearchCriteria searchCriteria) {
		
		PaginatedEntity<T> paginatedEntity=new PaginatedEntity<T>();
		
		paginatedEntity.setCurrentPage(searchCriteria.getPageNumber());
		paginatedEntity.setPageSize(searchCriteria.getPageSize());
		paginatedEntity.setNumberOfPages(baseEntities.getTotalPages());
		paginatedEntity.setNumberOfItems(baseEntities.getTotalElements());
		paginatedEntity.setHasNext(!baseEntities.isLast());
		paginatedEntity.setHasPrevious(!baseEntities.isFirst());
		paginatedEntity.setDataList(this.convertBasicListToBusinessList(baseEntities.getContent()));
		
		return paginatedEntity;
	
	}


}