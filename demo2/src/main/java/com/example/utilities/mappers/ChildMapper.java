package com.example.utilities.mappers;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.example.business.BaseBusinessEntity;
import com.example.db.BaseEntity;
@Component
public abstract class ChildMapper <S extends BaseEntity, T extends BaseBusinessEntity>{
	protected static final Logger logger = LoggerFactory.getLogger(Mapper.class);

	protected  abstract S convertBusinessUnitToBasicUnit(T t , boolean save);
	
	protected  abstract T convertBasicUnitToBusinessUnit(S s);
	
	
	public List<S> convertBusinessListToBasicList(List<T> businessEntities, boolean save) {
		List<S> entities = new ArrayList<S>();

		if (checkIfBusinessBasicListIsPopulated(businessEntities)) {
			for (T businessEntity : businessEntities) {
				entities.add(convertBusinessUnitToBasicUnit(businessEntity, save));
			}
		}

		return entities;
	}

	public List<T> convertBasicListToBusinessList(List<S> baseEntities) {
		List<T> businessEntities = new ArrayList<T>();

		if (checkIfBasicListIsPopulated(baseEntities)) {
			for (S baseEntity : baseEntities) {
				businessEntities.add(convertBasicUnitToBusinessUnit(baseEntity));
			}
		}

		return businessEntities;
	}

	
	public List<T> convertBasicSetToBusinessList(Set<S> baseEntities) {
		List<T> businessEntities = new ArrayList<T>();

		if (checkIfBasicSetIsPopulated(baseEntities)) {
			for (S baseEntity : baseEntities) {
				businessEntities.add(convertBasicUnitToBusinessUnit(baseEntity));
			}
		}

		return businessEntities;
	}

	
	public boolean checkIfBasicListIsPopulated(List<S> entities) {
		return (entities != null && !entities.isEmpty());
	}

	public boolean checkIfBusinessBasicListIsPopulated(List<T> businessEntities) {
		return (businessEntities != null && !businessEntities.isEmpty());
	}
	
	
	public boolean checkIfBasicSetIsPopulated(Set<S> entities) {
		return (entities != null && !entities.isEmpty());
	}

	public boolean checkIfBusinessBasicSetIsPopulated(Set<T> businessEntities) {
		return (businessEntities != null && !businessEntities.isEmpty());
	}
}
