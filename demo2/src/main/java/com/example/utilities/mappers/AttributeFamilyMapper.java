package com.example.utilities.mappers;

import org.springframework.stereotype.Component;

import com.example.business.BusinessAttributeFamily;
import com.example.db.AttributeFamily;
@Component
public class AttributeFamilyMapper extends ChildMapper<AttributeFamily, BusinessAttributeFamily> {

	@Override
	protected AttributeFamily convertBusinessUnitToBasicUnit(BusinessAttributeFamily businessAttributeFamily,
			boolean save) {
		AttributeFamily attributeFamily = new AttributeFamily();
		if (businessAttributeFamily != null) {
			attributeFamily.setId(businessAttributeFamily.getId());
			attributeFamily.setCode(businessAttributeFamily.getCode());
			attributeFamily.setName(businessAttributeFamily.getName());
			attributeFamily.setStatus(businessAttributeFamily.getStatus());
			attributeFamily.setRate(businessAttributeFamily.getRate());
			attributeFamily.setUserDefined(businessAttributeFamily.isUserDefined());
		}
		return attributeFamily;
	}

	@Override
	protected BusinessAttributeFamily convertBasicUnitToBusinessUnit(AttributeFamily attributeFamily) {
		BusinessAttributeFamily businessAttributeFamily = new BusinessAttributeFamily();
		if (attributeFamily != null) {
			businessAttributeFamily.setId(attributeFamily.getId());
			businessAttributeFamily.setCode(attributeFamily.getCode());
			businessAttributeFamily.setName(attributeFamily.getName());
			businessAttributeFamily.setStatus(attributeFamily.getStatus());
			businessAttributeFamily.setRate(attributeFamily.getRate());
			businessAttributeFamily.setUserDefined(attributeFamily.isUserDefined());
		}
		return businessAttributeFamily;
	}

}
