package com.example.utilities.mappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.example.business.BusinessUser;
import com.example.db.User;
import com.example.utilities.DateUtility;
@Component
public class UserMapper extends Mapper<User,BusinessUser> {

	@Autowired RoleMapper roleMapper;
	
	@Override
	public User convertBusinessUnitToBasicUnit(BusinessUser businessUser, boolean save) {
		
		User user = new User();
		if (businessUser != null) {
			user.setId(businessUser.getId());
			user.setEmail(businessUser.getEmail());
			user.setCreatedAt(save ? DateUtility.getCurrentDate() : businessUser.getCreatedAt());
			user.setUpdatedAt(DateUtility.getCurrentDate());
			user.setName(businessUser.getName());
			user.setPassword(businessUser.getPassword());
			user.setRememberToken(businessUser.getRememberToken());			
		}
		return user;
	}

	@Override
	public BusinessUser convertBasicUnitToBusinessUnit(User user) {
		BusinessUser businessUser = new BusinessUser();
		if (user != null) {
			businessUser.setId(user.getId());
			businessUser.setEmail(user.getEmail());
			businessUser.setCreatedAt(user.getCreatedAt());
			businessUser.setUpdatedAt(user.getUpdatedAt());
			businessUser.setName(user.getName());
			businessUser.setPassword(user.getPassword());
			businessUser.setRememberToken(user.getRememberToken());
			if(this.roleMapper.checkIfBasicSetIsPopulated(user.getRoles()))
			{
				businessUser.setBusinessRoles(this.roleMapper.convertBasicSetToBusinessList(user.getRoles()));
			}
			
		}
		return businessUser;
	}
}
