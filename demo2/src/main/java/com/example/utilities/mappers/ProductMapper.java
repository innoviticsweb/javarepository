package com.example.utilities.mappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import com.example.business.BusinessProduct;
import com.example.db.Product;
import com.example.utilities.DateUtility;

@Component
public class ProductMapper extends Mapper<Product, BusinessProduct> {

	@Autowired AttributeFamilyMapper attributeFamilyMapper;
	@Override
	public Product convertBusinessUnitToBasicUnit(BusinessProduct businessProduct, boolean save) {
		Product product=new Product();
		if(businessProduct!=null)
		{
			product.setId(businessProduct.getId());
			//product.setAttributeFamilyId(businessProduct.getAttributeFamilyId());
			if(businessProduct.getBusinessAttributeFamily()!=null)
			{				
				product.setAttributeFamily(this.attributeFamilyMapper.convertBusinessUnitToBasicUnit(businessProduct.getBusinessAttributeFamily(), save));
			}
			product.setParentId(businessProduct.getParentId());
			product.setSku(businessProduct.getSku());
			product.setType(businessProduct.getType());
			product.setUpdatedAt(businessProduct.getUpdatedAt());
			product.setCreatedAt(save?DateUtility.getCurrentDate():businessProduct.getCreatedAt());
			
		}
		return product;
	}

	@Override
	public BusinessProduct convertBasicUnitToBusinessUnit(Product product) {
		BusinessProduct businessProduct=new BusinessProduct();
		if(product!=null)
		{
			businessProduct.setId(product.getId());
			if(product.getAttributeFamily()!=null)
			{
				businessProduct.setBusinessAttributeFamily(this.attributeFamilyMapper.convertBasicUnitToBusinessUnit(product.getAttributeFamily()));
			}
			businessProduct.setParentId(product.getParentId());
			businessProduct.setSku(product.getSku());
			businessProduct.setType(product.getType());
			businessProduct.setUpdatedAt(product.getUpdatedAt());
			businessProduct.setCreatedAt(product.getCreatedAt());
		
		}
		return businessProduct;

	}

}
