package com.example.utilities.mappers;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.example.business.BusinessRole;
import com.example.db.Role;
import com.example.utilities.DateUtility;
@Component
public class RoleMapper extends ChildMapper<Role, BusinessRole>{

	@Override
	protected Role convertBusinessUnitToBasicUnit(BusinessRole businessRole, boolean save) {
		Role role =new Role();
		if(businessRole!=null)
		{
			role.setId(businessRole.getId());
			role.setName(businessRole.getName());
			role.setPermissions(businessRole.getPermissions());
			role.setPermissionType(businessRole.getPermissionType());
			role.setDescription(businessRole.getDescription());
			role.setUpdatedAt(DateUtility.getCurrentDate());
			role.setCreatedAt( save ? DateUtility.getCurrentDate() : businessRole.getCreatedAt());
		}
		return role;
		
	}

	@Override
	protected BusinessRole convertBasicUnitToBusinessUnit(Role role) {
		BusinessRole businessRole =new BusinessRole();
		if(role!=null)
		{
			businessRole.setId(role.getId());
			businessRole.setName(role.getName());
			businessRole.setPermissions(role.getPermissions());
			businessRole.setPermissionType(role.getPermissionType());
			businessRole.setDescription(role.getDescription());
			businessRole.setUpdatedAt(role.getUpdatedAt());
			businessRole.setCreatedAt(role.getCreatedAt());
		}
		return businessRole;
	}

}
