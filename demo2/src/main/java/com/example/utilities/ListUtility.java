package com.example.utilities;

import java.util.List;

import org.springframework.stereotype.Component;
@Component
public class ListUtility<T> {
	

	public boolean isListPopulated(List<T> dataList)
	{
		return (dataList!=null&&!dataList.isEmpty());
	}
	
	
	public boolean isListEmptyOrNull(List<T> dataList)
	{
		return (dataList==null||dataList.isEmpty());
	}
	
	public boolean isPaginatedListPopulated(PaginatedEntity<T> paginatedList)
	{
		return paginatedList!=null&&this.isListPopulated(paginatedList.getDataList());
	}
	
	public boolean isPaginatedListEmpty(PaginatedEntity<T> paginatedList)
	{
		return paginatedList==null||this.isListEmptyOrNull(paginatedList.getDataList());
	}
	
}
