package com.example.utilities;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public final class StringUtility {
	
	public static final String SUCCESS="success";
	public static final int SUCCESS_CODE=0;
	public static String getClassName(Object object)
	{
		return object.getClass().getName();
	}
	
	public static Boolean isStringPopulated(String string)
	{
		return (string!=null&&!string.isEmpty()&&!string.equals(""));
	}
	
	public static Boolean isStringArrayPopulated(String[] strings)
	{
		return (strings!=null&&strings.length!=0);
	}
	
	public static String convertToJson(ErrorCode errorCode) throws JsonProcessingException {
	       
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return mapper.writeValueAsString(errorCode.getMessage());
    }
	
	public static String addLeadingAndTrailingSpaces(String string)
	{
		return " "+string+" ";
	}
	
	public static String surroundWithBrackets(String[] strings)
	{
		return " in ("+String.join(",", strings)+")";
	}
	public static String surroundForLike(String string)
	{
		return " like '%"+string+"%'";
	}
}
