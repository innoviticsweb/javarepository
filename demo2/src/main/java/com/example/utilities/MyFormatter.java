package com.example.utilities;

import java.util.logging.Formatter;
import java.util.logging.LogRecord;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

public class MyFormatter extends Formatter {
	@Autowired
	private Environment env;
    @Override
    public String format(LogRecord record) {
        // TODO Auto-generated method stub
        StringBuilder r = new StringBuilder();
        r.append("<")
        	.append("-"+ DateUtility.getCurrentTimeStamp().toString()+"-")
            .append(">")
            .append(" ")
            .append(record.getSourceClassName())
            .append(" ")
            .append(record.getSourceMethodName())
            .append(" ")
            .append(record.getThreadID())
            .append(" ")
            .append(record.getLevel())
            .append(" ")
            .append(record.getMessage())
            .append(System
            .getProperty("line.separator"));

            return r.toString();
    }

    public String  getValue(String key)
    {
    	return this.env.getProperty(key);
    }

}