package com.example.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

public final class DateUtility {

	public static Date getCurrentDate()
	{
		return new Date();
	}
	
	public static Date getCurrentDateWithOutTimeStamp()
	{
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date dateWithoutTime=new Date();
		try 
		{
			dateWithoutTime = simpleDateFormat.parse(simpleDateFormat.format(new Date()));
		} 
		catch (ParseException e) 
		{
			e.printStackTrace();
		}
		return dateWithoutTime;
	}
	
	public static Integer getCurrentYear()
	{
		return ((new Date()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate()).getYear();		
	}
	public static Integer getCurrentMonth()
	{
		return ((new Date()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate()).getMonthValue();
	}
	public static Integer getCurrentDay()
	{
		return ((new Date()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate()).getDayOfMonth();
	}

	public static String getCurrentYearMonthDay()
	{
		return getCurrentYear().toString()+"-"+getCurrentMonth().toString()+"-"+getCurrentDay().toString();
	}
	
	
	public static String getCurrentTimeStamp() {
	    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");//dd/MM/yyyy
	    Date now = new Date();
	    String stringDate = simpleDateFormat.format(now);
	    return stringDate;
	}
}
