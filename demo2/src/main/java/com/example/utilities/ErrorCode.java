package com.example.utilities;

public enum ErrorCode {
	INTEGRATION_TEST_FAILED(998,"Integration Test Failed"),
	FAILED_TO_INTEGRATE(999,"Operation Could not be performed"),
	OPERATION_NOT_PERFORMED(1000,"Operation Could not be performed"),
	NO_DATA_FOUND(1001,"Could not find data"),
	NO_PRODUCTS_FOUND(1111, "No products were found."),
	PRODUCT_NOT_FOUND(1112,"Product was not found"),
	PRODUCT_NOT_SAVED(1113,"Product was not saved"),
	PRODUCT_NOT_UPDATED(1114,"Product was not updated"),
	PRODUCT_EXISTS(1115,"Product already exists"),
	NO_USERS_FOUND(2111, "No users were found."),
	USER_NOT_FOUND(2112, "User was not found."),
	USER_NOT_SAVED(2113, "User was not saved."),
	USER_NOT_UPDATED(2114, "User was not updated."),
	USER_EXISTS(2115, "User already exists."),
	INVALID_CREDENTIALS(2116, "Invalid username or password."),
	BAD_CREDENTIALS(2117, "Bad credentials."),
	FAILED_TO_VALIDATE_TOKEN(2118,"Invalid Token.")
	;
	
	private final int code;
	private final String message;
	

	  private ErrorCode(int code, String message) {
	    this.code = code;
	    this.message = message;
	  }

	  public String getMessage() {
	     return message;
	  }

	  public int getCode() {
	     return code;
	  }

	  @Override
	  public String toString() {
	    return code + ": " + message;
	  }
	  
	  
}
