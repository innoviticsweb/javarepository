package com.example.dbutility;

import org.springframework.stereotype.Component;

import com.example.db.AttributeFamily;
import com.example.db.Product;
@Component
public class ProductSpecification extends EntitySpecification<Product, AttributeFamily> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
