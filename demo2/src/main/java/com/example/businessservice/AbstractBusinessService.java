package com.example.businessservice;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.business.BusinessSearchCriteria;
import com.example.business.BusinessSearchOperation;
import com.example.business.SearchFilter;
import com.example.dbutility.DatabaseConditions;
import com.example.dbutility.SearchCriteria;
import com.example.dbutility.SearchOperation;
import com.example.exceptions.BusinessException;
import com.example.exceptions.IntegrationException;
import com.example.utilities.ArrayUtility;
import com.example.utilities.ErrorCode;
import com.example.utilities.ExceptionHandler;
import com.example.utilities.ListUtility;
import com.example.utilities.StringUtility;
@Service
public abstract class AbstractBusinessService <T> {
	protected static final Logger logger = LoggerFactory.getLogger(AbstractBusinessService.class);
	@Autowired
	protected ExceptionHandler exceptionHandler;
	@Autowired
	protected ArrayUtility arrayUtility;

	
	protected boolean isExceptionOfTypeEntityNotFoundException(Exception exception)
	{
		boolean result=exception instanceof EntityNotFoundException||(exception.getMessage()!=null&&exception.getMessage().contains("EntityNotFoundException"));
		this.logger.info("exception instanceof EntityNotFoundException:::"+result);
		return result;
	}
	
	protected boolean isExceptionOfTypeNoResultException(Exception exception)
	{
		boolean result=exception instanceof EntityNotFoundException||(exception.getMessage()!=null&&exception.getMessage().contains("NoResultException"));
		this.logger.info("exception instanceof NoResultException:::"+result);
		return (result);
	}
	
	protected  BusinessException handleBusinessException(Exception exception,ErrorCode errorCode)
	{
		if (this.isExceptionOfTypeEntityNotFoundException(exception)||this.isExceptionOfTypeNoResultException(exception))
			return this.exceptionHandler.handleAsBusinessException(exception, errorCode);
		
		else
		return this.exceptionHandler.handleAsBusinessException(exception, ErrorCode.OPERATION_NOT_PERFORMED);
	}
	

	
	protected  BusinessException handleIntegrationException(IntegrationException integrationException,ErrorCode errorCode)
	{
		this.logger.info("Handling Exception as an Integration Exception::::"+integrationException.getMessage());
		return this.exceptionHandler.handleIntegrationExceptionAsBusinessException(integrationException, errorCode);
	}
	
	public DatabaseConditions generateDatabaseConditions(BusinessSearchCriteria businessSearchCriteria)
	{
		DatabaseConditions databaseConditions=new DatabaseConditions();
		if(businessSearchCriteria!=null)
		{
			if(businessSearchCriteria.getPageNumber()!=null&&businessSearchCriteria.getPageSize()!=null)
			{
				databaseConditions.setPageRequest(PageRequest.of(businessSearchCriteria.getPageNumber()-1,businessSearchCriteria.getPageSize()));
			}
			if(businessSearchCriteria.getPageNumber()!=null&&businessSearchCriteria.getPageSize()!=null
					&&businessSearchCriteria.getAsc()!=null&&
					StringUtility.isStringPopulated(businessSearchCriteria.getSortingParam()))
			{
				if(businessSearchCriteria.getAsc())
				{
					Sort sort = Sort.by(businessSearchCriteria.getSortingParam()).ascending();
					databaseConditions.setPageRequest(PageRequest.of(businessSearchCriteria.getPageNumber()-1,businessSearchCriteria.getPageSize(),sort));					
				}
				else
				{
					Sort sort = Sort.by(businessSearchCriteria.getSortingParam()).descending();
					databaseConditions.setPageRequest(PageRequest.of(businessSearchCriteria.getPageNumber()-1,businessSearchCriteria.getPageSize(),sort));					
				}				
			}
			if(this.arrayUtility.isArrayPopulated(businessSearchCriteria.getSearchesAndFilters()))
			{
				List<SearchCriteria> searchCriteriaList=new ArrayList<SearchCriteria>();
				
				for (int i=0;i<businessSearchCriteria.getSearchesAndFilters().length;i++)
				{
					SearchFilter searchFilter=businessSearchCriteria.getSearchesAndFilters()[i];
					
					if (!StringUtility.isStringPopulated(searchFilter.getParentColumn())) 
					{
						/*if (searchFilter.getSearch() != null && searchFilter.getSearch()) {
							searchCriteriaList.add(new SearchCriteria(searchFilter.getKey(), searchFilter.getValue(),
									SearchOperation.LIKE, null));
						} else if (searchFilter.getSearch() != null && !searchFilter.getSearch()) {
							searchCriteriaList.add(new SearchCriteria(searchFilter.getKey(), this.generateObjectListFromObjectArray(searchFilter.getValues()),
									SearchOperation.IN, null));
						}*/
						if (searchFilter.getOperation() != null && searchFilter.getOperation()==BusinessSearchOperation.SEARCH.getOperation()) {
							searchCriteriaList.add(new SearchCriteria(searchFilter.getKey(), searchFilter.getValue(),
									SearchOperation.LIKE, null));
						} else if (searchFilter.getOperation() != null && searchFilter.getOperation()==BusinessSearchOperation.FILTER.getOperation()) {
							searchCriteriaList.add(new SearchCriteria(searchFilter.getKey(), this.arrayUtility.generateObjectListFromObjectArray(searchFilter.getValues()),
									SearchOperation.IN, null));
						}
					}
					else
					{
						/*if (searchFilter.getSearch() != null && searchFilter.getSearch()) {
							searchCriteriaList.add(new SearchCriteria(searchFilter.getKey(), searchFilter.getValue(),
									SearchOperation.PARENT_LIKE, searchFilter.getParentColumn()));
						} else if (searchFilter.getSearch() != null && !searchFilter.getSearch()) {
							searchCriteriaList.add(new SearchCriteria(searchFilter.getKey(), this.generateObjectListFromObjectArray(searchFilter.getValues()),
									SearchOperation.PARENT_IN, searchFilter.getParentColumn()));
						}*/
						if (searchFilter.getOperation() != null && searchFilter.getOperation()==BusinessSearchOperation.SEARCH.getOperation()) {
							searchCriteriaList.add(new SearchCriteria(searchFilter.getKey(), searchFilter.getValue(),
									SearchOperation.PARENT_LIKE, searchFilter.getParentColumn()));
						} else if (searchFilter.getOperation() != null && searchFilter.getOperation()==BusinessSearchOperation.FILTER.getOperation()) {
							searchCriteriaList.add(new SearchCriteria(searchFilter.getKey(), this.arrayUtility.generateObjectListFromObjectArray(searchFilter.getValues()),
									SearchOperation.PARENT_IN, searchFilter.getParentColumn()));
						}
					}
					
				}
				databaseConditions.setSearchCriteria(searchCriteriaList);				
			}
		}
		
		
		return databaseConditions;
	}
	
	
	
	
}
