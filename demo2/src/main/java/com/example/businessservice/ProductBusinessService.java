package com.example.businessservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.example.business.BusinessProduct;
import com.example.business.BusinessSearchCriteria;
import com.example.db.Product;
import com.example.exceptions.BusinessException;
import com.example.service.ProductService;
import com.example.utilities.ErrorCode;
import com.example.utilities.PaginatedEntity;
import com.example.utilities.mappers.ProductMapper;

@Service
public class ProductBusinessService extends AbstractBusinessService<BusinessProduct>{

	@Autowired ProductService productService;
	@Autowired ProductMapper productMapper;
	public List<BusinessProduct> findAll() throws BusinessException
	{
		
		List<Product> products=new ArrayList<Product>();
		List<BusinessProduct> BusinessProducts=new ArrayList<BusinessProduct>();
		try {
		products=this.productService.findAll();		
		BusinessProducts= productMapper.convertBasicListToBusinessList(products);
		}
		catch(Exception exception)
		{
			throw this.handleBusinessException(exception,ErrorCode.NO_PRODUCTS_FOUND);
		}
		return BusinessProducts;
	}
	
	public BusinessProduct findById(int id) throws BusinessException 
	{
		Product product=new Product();
		BusinessProduct businessProduct=new BusinessProduct();
		try {
			product=productService.findById(id);
			businessProduct=productMapper.convertBasicUnitToBusinessUnit(product);
		}
		catch (Exception exception) 
		{
			throw this.handleBusinessException(exception,ErrorCode.PRODUCT_NOT_FOUND);
		}
		return businessProduct;
	}
	
	
	public BusinessProduct save(BusinessProduct BusinessProduct) throws BusinessException
	{
		Product Product=new Product();
		BusinessProduct savedBusinessProduct=new BusinessProduct();
		try {
			Product= productService.save(productMapper.convertBusinessUnitToBasicUnit(BusinessProduct,true));
			savedBusinessProduct=productMapper.convertBasicUnitToBusinessUnit(Product);
		}
		catch (Exception exception) 
		{
			throw this.handleBusinessException(exception,ErrorCode.PRODUCT_NOT_SAVED);
		}
		return savedBusinessProduct;
	}
	
	public BusinessProduct update(BusinessProduct BusinessProduct) throws BusinessException
	{
		Product Product =new Product();
		BusinessProduct updatedBusinessProduct=new BusinessProduct();
		try 
		{
			Product= productService.save(productMapper.convertBusinessUnitToBasicUnit(BusinessProduct,false));
			updatedBusinessProduct=productMapper.convertBasicUnitToBusinessUnit(Product);
		}
		catch(Exception exception)
		{
			throw this.handleBusinessException(exception,ErrorCode.PRODUCT_NOT_UPDATED);
		}
		
		
		return updatedBusinessProduct;
	}
	
	public List<BusinessProduct> findAllByParentId(int id) throws BusinessException
	{
		
		List<Product> products=new ArrayList<Product>();
		List<BusinessProduct> businessProducts=new ArrayList<BusinessProduct>();
		try {
		products=this.productService.findByParam(id);
		this.logger.info("DB Products:::"+products);
		businessProducts= productMapper.convertBasicListToBusinessList(products);
		this.logger.info("Business Products:::"+businessProducts);
		}
		catch(Exception exception)
		{
		    throw this.handleBusinessException(exception,ErrorCode.NO_PRODUCTS_FOUND);
		}
		return businessProducts;
	}

	public PaginatedEntity<BusinessProduct> findAllByParentIdPaginated(BusinessSearchCriteria businessSearchCriteria) throws BusinessException
	{
		PaginatedEntity<BusinessProduct> businessProducts=new PaginatedEntity<BusinessProduct>();
		try 
		{
				//Page<Product> products=	this.productService.findByParamPaginated(searchCriteria);
			
				Page<Product> products= this.productService.findByParamPaginated(this.generateDatabaseConditions(businessSearchCriteria));
				this.logger.info("DB Products:::");
				businessProducts= productMapper.convertBasicPageToBusinessPage(products,businessSearchCriteria);
				this.logger.info("Business Products:::"+businessProducts);
		}
		catch(Exception exception)
		{
		    throw this.handleBusinessException(exception,ErrorCode.NO_PRODUCTS_FOUND);
		}
		return businessProducts;
	}

	
}
