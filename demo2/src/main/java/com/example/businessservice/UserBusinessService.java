package com.example.businessservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.business.BusinessUser;
import com.example.db.User;
import com.example.exceptions.BusinessException;
import com.example.exceptions.GeneralException;
import com.example.restentities.external.AddRestUserRequest;
import com.example.service.UserService;
import com.example.utilities.ErrorCode;
import com.example.utilities.mappers.Mapper;
import com.example.utilities.mappers.UserMapper;

@Service
public class UserBusinessService extends AbstractBusinessService<BusinessUser> {
	
	@Autowired UserService userService;
	@Autowired UserMapper userMapper;
	public List<BusinessUser> findAll() throws BusinessException 
	{
		
		List<User> users=new ArrayList<User>();
		List<BusinessUser> businessUsers=new ArrayList<BusinessUser>();
		try {
		users=this.userService.findAll();		
		businessUsers= userMapper.convertBasicListToBusinessList(users);
		}
		catch(Exception exception)
		{
		    throw this.handleBusinessException(exception,ErrorCode.NO_USERS_FOUND);
		}
		return businessUsers;
	}
	
	public BusinessUser findById(int id) throws BusinessException 
	{
		User user=new User();
		BusinessUser businessUser=new BusinessUser();
		try {
			user=userService.findById(id);
			businessUser=userMapper.convertBasicUnitToBusinessUnit(user);
		}
		catch (Exception exception) 
		{
			throw this.handleBusinessException(exception,ErrorCode.USER_NOT_FOUND);
		}
		return businessUser;
	}
	
	
	public BusinessUser save(BusinessUser businessUser) throws BusinessException 
	{
		User user=new User();
		BusinessUser savedbusinessUser=new BusinessUser();
		try {
			user= userService.save(userMapper.convertBusinessUnitToBasicUnit(businessUser,true));
			savedbusinessUser=userMapper.convertBasicUnitToBusinessUnit(user);
		}
		catch (Exception exception) 
		{
			throw this.handleBusinessException(exception,ErrorCode.USER_NOT_SAVED);
		}
		return savedbusinessUser;
	}
	
	public BusinessUser update(BusinessUser businessUser) throws BusinessException  
	{
		User user =new User();
		BusinessUser updatedbusinessUser=new BusinessUser();
		try 
		{
			user= userService.save(userMapper.convertBusinessUnitToBasicUnit(businessUser,false));
			updatedbusinessUser=userMapper.convertBasicUnitToBusinessUnit(user);
		}
		catch(Exception exception)
		{
			throw this.handleBusinessException(exception,ErrorCode.USER_NOT_UPDATED);
		}
		
		
		return updatedbusinessUser;
	}
	
	public List<BusinessUser> findAllByPassword(String password) throws BusinessException 
	{
		
		List<User> users=new ArrayList<User>();
		List<BusinessUser> businessUsers=new ArrayList<BusinessUser>();
		try {
		users=this.userService.findByParam(password);		
		businessUsers= userMapper.convertBasicListToBusinessList(users);
		}
		catch(Exception exception)
		{
			throw this.handleBusinessException(exception,ErrorCode.NO_USERS_FOUND);
		}
		return businessUsers;
	}

	public BusinessUser findByName(String username) throws BusinessException 
	{
		
		BusinessUser businessUser=new BusinessUser();
		try {
			businessUser=userMapper.convertBasicUnitToBusinessUnit(this.userService.findByUsername(username));
		}
		catch(Exception exception)
		{
			throw this.handleBusinessException(exception,ErrorCode.NO_USERS_FOUND);
		}
		return businessUser;
	}
	public BusinessUser findByUsernameAndPassword(String username,String password) throws BusinessException 
	{
		
		BusinessUser businessUser=new BusinessUser();
		try {
			businessUser=userMapper.convertBasicUnitToBusinessUnit(this.userService.findByUsernameAndPassword(username,password));
		}
		catch(Exception exception)
		{
			throw this.handleBusinessException(exception,ErrorCode.INVALID_CREDENTIALS);
		}
		return businessUser;
	}
	
}
