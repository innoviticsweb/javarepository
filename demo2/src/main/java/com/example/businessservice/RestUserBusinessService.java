package com.example.businessservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.business.RestUserBusiness;
import com.example.exceptions.BusinessException;
import com.example.exceptions.IntegrationException;
import com.example.restutilities.AddRestUserMapper;
import com.example.restutilities.GetRestUserMapper;
import com.example.utilities.ErrorCode;
@Service
public class RestUserBusinessService extends AbstractBusinessService<RestUserBusiness> {
	
	@Autowired AddRestUserMapper addRestUserMapper;
	@Autowired GetRestUserMapper getRestUserMapper;
	
	public RestUserBusiness save(RestUserBusiness restUserBusiness) throws  BusinessException
	{
		RestUserBusiness savedRestUserBusiness=new RestUserBusiness();
		try
		{
			savedRestUserBusiness= this.addRestUserMapper.wrapBaseBusinessEntity(false,restUserBusiness,null).getData();
		}
		catch (IntegrationException exception) {
			throw this.handleIntegrationException(exception,ErrorCode.USER_NOT_SAVED);
		}
		return savedRestUserBusiness;
	}
	
	public List<RestUserBusiness> list() throws  BusinessException
	{
		List<RestUserBusiness> restUserBusinesses= new ArrayList<RestUserBusiness>(); 
		logger.info("List Method::::");
		try
		{
			restUserBusinesses= this.getRestUserMapper.wrapBaseBusinessEntity(true,new RestUserBusiness(),null).getDataList();
		}
		catch (IntegrationException exception) {
			throw this.handleIntegrationException(exception,ErrorCode.NO_USERS_FOUND);
		}
		return restUserBusinesses;
	}
	

	public RestUserBusiness getById(String params) throws  BusinessException
	{
		RestUserBusiness restUserBusiness= new RestUserBusiness(); 
		try
		{
			restUserBusiness= this.getRestUserMapper.wrapBaseBusinessEntity(false,new RestUserBusiness(),params).getData();
		}
		catch (IntegrationException exception) {
			throw this.handleIntegrationException(exception,ErrorCode.USER_NOT_FOUND);
		}
		return restUserBusiness;
	}
	
}
