package com.example.restentities;

import java.util.List;

public class BaseGenericRestResponse <T> {
	
	T data;
	
	List <T> dataList;

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public List<T> getDataList() {
		return dataList;
	}

	public void setDataList(List<T> dataList) {
		this.dataList = dataList;
	}
	

}
