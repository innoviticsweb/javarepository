package com.example.restentities.external;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AddRestUserRequest extends RestUserRequest{ 

	private int id;
    private String name;
    private String email;
    private String gender;
    private String status;
    
    public AddRestUserRequest(
    		@JsonProperty("id")int id,
    		@JsonProperty("name") String name,
    		@JsonProperty("email")String email,
    		@JsonProperty("gender")String gender,
    		@JsonProperty("status")String status) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.gender = gender;
		this.status = status;
	}
    public AddRestUserRequest() {
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
    
    
    
}
