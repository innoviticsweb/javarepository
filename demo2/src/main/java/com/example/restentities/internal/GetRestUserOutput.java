package com.example.restentities.internal;

import java.util.List;

public class GetRestUserOutput extends RestUserOutput{

	List<SingleRestUserOutput> restUserOutputs;

	SingleRestUserOutput restUserOutput;
	
	public List<SingleRestUserOutput> getRestUserOutputs() {
		return restUserOutputs;
	}

	public void setRestUserOutputs(List<SingleRestUserOutput> restUserOutputs) {
		this.restUserOutputs = restUserOutputs;
	}

	public SingleRestUserOutput getRestUserOutput() {
		return restUserOutput;
	}

	public void setRestUserOutput(SingleRestUserOutput restUserOutput) {
		this.restUserOutput = restUserOutput;
	}

	

	
	
	
	
}
