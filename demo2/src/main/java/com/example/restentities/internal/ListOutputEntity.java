package com.example.restentities.internal;

import java.util.List;

public class ListOutputEntity <SO> {

	List<SO> outputEntities;

	public List<SO> getOutputEntities() {
		return outputEntities;
	}

	public void setOutputEntities(List<SO> outputEntities) {
		this.outputEntities = outputEntities;
	}

	boolean isListNotEmpty()
	{
		return (outputEntities!=null&&!outputEntities.isEmpty());
	}
	
}
