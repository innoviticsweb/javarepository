package com.example.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;
import javax.persistence.Query;

import com.example.business.BusinessSearchCriteria;
import com.example.db.Product;
import com.example.utilities.PaginatedEntity;

public class ProductRepositoryImpl extends AbstractRepository<Product> implements ProductRepositoryCustom {

	@SuppressWarnings("unchecked")
	@Override
	public List<Product> findByParam(Integer param) {
		/*Query query=entityManager.createNativeQuery("Select p.*,af.* from Products p inner join attribute_families af on "
				+ " af.id =?"
			  + " and p.attribute_family_id=af.id", Product.class) ;
		       query.setParameter(1, param);
		
		return (List<Product>)query.getResultList();
		*/
		
		 return (List<Product>)this.
		 generateQuery("Select p.*,af.* from Products p inner join attribute_families af on af.id =?"
		 + " and p.attribute_family_id=af.id", Product.class,String.valueOf(param)).getResultList();		
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public Page<Product>  findByParamPaginated(BusinessSearchCriteria searchCriteria) {

		String queryString="Select p.*,af.* from Products p inner join attribute_families af on p.attribute_family_id=af.id ";
		this.generateQuery(queryString, null, null);
		
		/*
		 * PaginatedEntity<Product> paginatedEntity=
		 * this.createPaginatedEntity(this.generateQuery(queryString, Product.class,
		 * String.valueOf(param)).getResultList(), pageNumber, pageSize);
		 */		
		
		//return paginatedEntity;
		return null;
	
	}

}
