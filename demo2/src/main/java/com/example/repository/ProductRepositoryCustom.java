package com.example.repository;

import java.util.List;

import org.springframework.data.domain.Page;

import com.example.business.BusinessSearchCriteria;
import com.example.db.Product;
import com.example.utilities.PaginatedEntity;

public interface ProductRepositoryCustom {
	List<Product> findByParam(Integer param);
	
	Page<Product>  findByParamPaginated(BusinessSearchCriteria searchCriteria);
}
