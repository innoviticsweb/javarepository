package com.example.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.business.BusinessSearchCriteria;
import com.example.db.Product;
import com.example.utilities.PaginatedEntity;

@Repository
public interface ProductRepository extends JpaRepository<Product,Integer>,ProductRepositoryCustom {	
	//@Query(value="Select p.*,af.* from Products p inner join attribute_families af on  af.id =? and p.attribute_family_id=af.id",nativeQuery =true)
	List<Product> findByParam(Integer param);
	Page<Product> findByParamPaginated(BusinessSearchCriteria searchCriteria);
	//@Query(value="Select p.*,af.* from Products p inner join attribute_families af on  af.id =? and p.attribute_family_id=af.id",nativeQuery =true)	
	//Page<Product> findByParamPaginated(Integer param, Pageable pageable);
	
	//Page<Product> findByParamFiltered(Integer param, Pageable pageable);
}
