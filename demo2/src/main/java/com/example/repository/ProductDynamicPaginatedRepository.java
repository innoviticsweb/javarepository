package com.example.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.db.Product;
@Repository
public interface ProductDynamicPaginatedRepository  extends CrudRepository<Product, Long>, JpaSpecificationExecutor<Product> {
}
