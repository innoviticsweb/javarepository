package com.example.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.db.Product;

public interface PaginatedProductRepository extends JpaRepository<Product, Integer>  {

	@Query(value="Select p.*,af.* from products p inner join attribute_families af on p.attribute_family_id=af.id where p.attribute_family_id in  (?)",
			countQuery = "Select count(*) from products p inner join attribute_families af on p.attribute_family_id=af.id where p.attribute_family_id in  (?)",
			nativeQuery =true)	
	Page<Product> findByParamPaginated(String conditions, Pageable pageable);
		
}
