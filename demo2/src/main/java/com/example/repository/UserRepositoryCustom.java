package com.example.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;

import com.example.db.User;

public interface UserRepositoryCustom {

	List<User> findByParam(String param);
	
	User findByUsername(String username);
	
	User findByUsernameAndPassword(String username,String password);
}
