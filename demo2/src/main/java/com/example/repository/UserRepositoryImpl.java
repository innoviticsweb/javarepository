package com.example.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.stereotype.Repository;

import com.example.db.User;
@Repository
public class UserRepositoryImpl extends AbstractRepository<User> implements UserRepositoryCustom{



	@Override
	public List<User> findByParam(String param) {
		return (List<User>)this.generateQuery("Select * from users where password= ?", User.class, param).getResultList();
		
	}
	@Override
	public User findByUsername(String username) {
		return (User) this.generateQuery("Select * from users where name= ?",User.class, username).getSingleResult();
	}

	@Override
	public User findByUsernameAndPassword(String username, String password) {
		return (User) this.generateQuery("Select * from users where name= ? and password=?",User.class, username,password).getSingleResult();
	}
	
}
