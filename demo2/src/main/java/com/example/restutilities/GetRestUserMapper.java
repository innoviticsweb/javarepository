package com.example.restutilities;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.business.RestUserBusiness;
import com.example.exceptions.IntegrationException;
import com.example.restapiconsumer.user.GetRestUserApiConsumer;
import com.example.restapiconsumer.user.GetSingleRestUserApiConsumer;
import com.example.restentities.external.GetRestUserResponse;
import com.example.restentities.external.SingleGetRestUserResponse;
import com.example.restentities.internal.GetRestUserInput;
import com.example.restentities.internal.GetRestUserOutput;
import com.example.restentities.internal.SingleRestUserOutput;
import com.example.utilities.ListUtility;
@Service
public class GetRestUserMapper extends RestMapper<GetRestUserInput, GetRestUserOutput, GetRestUserResponse,  RestUserBusiness> {
	@Autowired
	GetRestUserApiConsumer getRestUserApiConsumer;
	@Autowired
	GetSingleRestUserApiConsumer getSingleRestUserApiConsumer;
	@Autowired ListUtility<SingleRestUserOutput> listUtility;
	@Override
	public RestUserBusiness consumeRestService(RestUserBusiness restUserBusiness,String params) throws IntegrationException {
		return this.createBusinessEntityFromOutput(this.getSingleRestUserApiConsumer.invoke(this.createInput(restUserBusiness), SingleGetRestUserResponse.class, params));
	}
	
	@Override
	public List<RestUserBusiness> consumeListRestService(RestUserBusiness restUserBusiness, String params) throws IntegrationException {
		return this.createListBusinessEntityFromOutput(this.getRestUserApiConsumer.invoke(this.createInput(restUserBusiness), SingleGetRestUserResponse[].class, params));
	}
	

	@Override
	GetRestUserInput createInput(RestUserBusiness restUserBusiness) {
		return null;
	}

	@Override
	RestUserBusiness createBusinessEntityFromOutput(GetRestUserOutput getRestUserOutput) {
		
		return getRestUserOutput!=null?this.getConversion(getRestUserOutput.getRestUserOutput()):new RestUserBusiness();
	}

	
	@Override
	List<RestUserBusiness> createListBusinessEntityFromOutput(GetRestUserOutput getRestUserOutput) {
		List<RestUserBusiness> restUserBusinesses=new ArrayList<RestUserBusiness>();
		if( getRestUserOutput!=null&&this.listUtility.isListPopulated(getRestUserOutput.getRestUserOutputs()))	
		{
			for(SingleRestUserOutput singleRestUserOutput: getRestUserOutput.getRestUserOutputs())
			{
				restUserBusinesses.add(this.getConversion(singleRestUserOutput));
			}
			
		}
		return restUserBusinesses; 
	}
	
	
	RestUserBusiness getConversion(SingleRestUserOutput singleRestUserOutput)
	{
		RestUserBusiness restUserBusiness=new RestUserBusiness();
		if(singleRestUserOutput!=null)
		{
			restUserBusiness.setId(singleRestUserOutput.getId());
			restUserBusiness.setEmail(singleRestUserOutput.getEmail());
			restUserBusiness.setGender(singleRestUserOutput.getGender());
			restUserBusiness.setName(singleRestUserOutput.getName());
			restUserBusiness.setStatus(singleRestUserOutput.getStatus());
		}
		return restUserBusiness; 
		
	}

	
	
}
