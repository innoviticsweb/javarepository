package com.example.restutilities;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.example.business.RestUserBusiness;
import com.example.exceptions.IntegrationException;
import com.example.restapiconsumer.user.AddRestUserApiConsumer;
import com.example.restentities.external.AddRestUserResponse;
import com.example.restentities.internal.AddRestUserInput;
import com.example.restentities.internal.AddRestUserOutput;
@Service
public class AddRestUserMapper extends RestMapper<AddRestUserInput, AddRestUserOutput, AddRestUserResponse,  RestUserBusiness>{
	@Autowired
	AddRestUserApiConsumer addRestUserApiConsumer;
	@Override
	public RestUserBusiness consumeRestService(RestUserBusiness restUserBusiness,String params) throws IntegrationException {

		AddRestUserOutput output = new AddRestUserOutput();
		output = this.addRestUserApiConsumer.invoke(this.createInput(restUserBusiness), AddRestUserResponse.class,null);
		return this.createBusinessEntityFromOutput(output);

	}

	@Override
	AddRestUserInput createInput(RestUserBusiness restUserBusiness) {
		AddRestUserInput input=new AddRestUserInput();
		if(restUserBusiness!=null)
		{
			input.setId(restUserBusiness.getId());
			input.setGender(restUserBusiness.getGender());
			input.setName(restUserBusiness.getGender());
			input.setStatus(restUserBusiness.getStatus());
			input.setEmail(restUserBusiness.getEmail());
		}
		return input;
	}


	@Override
	RestUserBusiness createBusinessEntityFromOutput(AddRestUserOutput output) {
		RestUserBusiness restUserBusiness=new RestUserBusiness();
		if(output!=null)
		{
			restUserBusiness.setId(output.getId());
			restUserBusiness.setGender(output.getGender());
			restUserBusiness.setName(output.getGender());
			restUserBusiness.setStatus(output.getStatus());
			restUserBusiness.setEmail(output.getEmail());
		}
		return restUserBusiness;
	}

	@Override
	List<RestUserBusiness> createListBusinessEntityFromOutput(AddRestUserOutput BaseOutput) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RestUserBusiness> consumeListRestService(RestUserBusiness baseBusinessEntity, String params)
			throws IntegrationException {
		// TODO Auto-generated method stub
		return null;
	}
}
