package com.example.restutilities;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.business.BaseBusinessEntity;
import com.example.business.WrapperBusinessEntity;
import com.example.exceptions.IntegrationException;
import com.example.restentities.BaseInput;
import com.example.restentities.BaseOutput;
import com.example.restentities.BaseRestResponse;
import com.example.utilities.ErrorCode;
import com.example.utilities.ExceptionHandler;
import com.example.utilities.ListUtility;
@Service
public abstract class RestMapper <I extends BaseInput,O extends BaseOutput, RES extends BaseRestResponse ,B extends BaseBusinessEntity>{
	public final static Logger logger = LogManager.getLogger(RestMapper.class.getName());
	@Autowired
	ExceptionHandler exceptionHandler;
	@Autowired
	ListUtility<B> listUtility;
	
	
	abstract B consumeRestService(B baseBusinessEntity,String params) throws IntegrationException;	
	
	abstract List<B> consumeListRestService(B baseBusinessEntity,String params) throws IntegrationException;
	
	abstract I createInput (B baseBusinessEntity) ;
	
	abstract B createBusinessEntityFromOutput(O BaseOutput);
	
	abstract List<B> createListBusinessEntityFromOutput(O BaseOutput);
	
	
	public WrapperBusinessEntity<B> wrapBaseBusinessEntity(boolean isList,B baseBusinessEntity,String params) throws IntegrationException
	{
		WrapperBusinessEntity<B> wrapperBusinessEntity=new WrapperBusinessEntity<B>();
		try {
			if (!isList) {
				wrapperBusinessEntity.setData(this.consumeRestService(baseBusinessEntity, params));
			} else {
				logger.info("List Wrapper:::::");
				wrapperBusinessEntity.setDataList(this.consumeListRestService(baseBusinessEntity, params));
			}
		} catch (Exception exception) {
			this.logger.info("Exception Stack Trace:::");
			exception.printStackTrace();
			throw this.exceptionHandler.handleAsIntegrationException(exception, ErrorCode.INTEGRATION_TEST_FAILED);
		}
		return wrapperBusinessEntity;
	}
	
}
