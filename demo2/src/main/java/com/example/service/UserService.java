package com.example.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.db.User;
import com.example.repository.UserRepository;
@Service
public class UserService extends AbstractService<User,String> implements IUserService {

	@Autowired(required = true)
	private	UserRepository userRepository;
	
	
	@Override
	public List<User> findAll() {
	
		return this.userRepository.findAll();
	
	}
	
	@Override
	public User save(User user) {
	
		return this.userRepository.save(user);
	
	}
	
	
	@Override
	public User update(User user) {
		
		return this.userRepository.save(user);
		
	}
	
	@Override
	public User findById(int id){
		User user = new User();

		user = this.userRepository.getById(id);

		return user;
	
	}
	

	@Override
	public List<User> findByParam(String parameter) {
		List<User> users= new ArrayList<User>();
		users=this.userRepository.findByParam(parameter);
		return users;
	}

	@Override
	public User findByUsername(String param) {
		User user=new User();
		user=this.userRepository.findByUsername(param);
		return user;
	}
	
	@Override
	public User findByUsernameAndPassword(String username,String password) {
		User user=new User();
		user=this.userRepository.findByUsernameAndPassword(username,password);
		return user;
	}
}
