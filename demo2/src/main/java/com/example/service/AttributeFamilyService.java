package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.db.AttributeFamily;
import com.example.repository.AttributeFamilyRepository;

public class AttributeFamilyService extends AbstractService<AttributeFamily, Integer> implements IAttributeFamilyService{

	@Autowired  AttributeFamilyRepository attributeFamilyRepository;
	@Override
	public List<AttributeFamily> findAll() {
		
		return this.attributeFamilyRepository.findAll();
	}

	@Override
	public AttributeFamily findById(int id) {
		
		return this.attributeFamilyRepository.getById(id);
	}

	@Override
	public AttributeFamily save(AttributeFamily attributeFamily) {
		
		return this.attributeFamilyRepository.save(attributeFamily);
	}

	@Override
	public AttributeFamily update(AttributeFamily attributeFamily) {
		
		return this.attributeFamilyRepository.save(attributeFamily);
	}

	@Override
	public List<AttributeFamily> findByParam(Integer param) {
		// TODO Auto-generated method stub
		return null;
	}
		
	
	
}
