package com.example.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.example.db.BaseEntity;
@Service
public  class BaseSearchSpecification<T extends BaseEntity,P extends BaseEntity> {
	
	public  Specification<T> constructInSpecification(BaseSearchCriteria baseSearchCriteria) {
		return new Specification<T>() {
			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				if (baseSearchCriteria.getParentIds() != null && !baseSearchCriteria.getParentIds().isEmpty()) {
					Join<T, P> attributeFamilies = root.join(baseSearchCriteria.getJoinColumn());
					predicates.add(attributeFamilies.in(baseSearchCriteria.getParentIds()));
				}
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				
			 }

		};
	}
	
	
	public  Specification<T> constructEqualSpecification(BaseSearchCriteria baseSearchCriteria) {
	      return new Specification<T>() {
			private static final long serialVersionUID = 1L;
			@Override
	          public Predicate toPredicate(Root<T> root,
	                                       CriteriaQuery<?> query,
	                                       CriteriaBuilder criteriaBuilder) {
	              Predicate equalPredicate = criteriaBuilder.equal(root.<String>get(baseSearchCriteria.getKeyName()), baseSearchCriteria.getKeyValue());
	              return equalPredicate;
	          }
	      };
	  }
		
}