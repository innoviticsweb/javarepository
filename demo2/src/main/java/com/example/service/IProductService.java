package com.example.service;

import java.util.List;

import com.example.db.Product;
import com.example.db.User;

public interface IProductService {

	public List<Product> findAll();
	public Product findById(int id);
	public Product save(Product product);
	public Product update(Product product);
	public List<Product> findByParam(Integer param);  
}
