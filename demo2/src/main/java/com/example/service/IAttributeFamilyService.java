package com.example.service;

import java.util.List;

import com.example.db.AttributeFamily;

public interface IAttributeFamilyService {

	public List<AttributeFamily> findAll();
	public AttributeFamily findById(int id);
	public AttributeFamily save(AttributeFamily attributeFamily);
	public AttributeFamily update(AttributeFamily attributeFamily);	
	public List<AttributeFamily> findByParam(Integer param);
}
