package com.example.service;

import java.util.List;

import com.example.db.User;
import com.example.restentities.external.AddRestUserRequest;

public interface IUserService {
	
	public List<User> findAll();
	public User findById(int id);
	public User save(User user);
	public User update(User user);	
	public List<User> findByParam(String param);
	public User findByUsername(String param);
	public User findByUsernameAndPassword(String username,String password);
}
