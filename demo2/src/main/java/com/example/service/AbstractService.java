package com.example.service;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
/*import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
*/import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.business.BusinessSearchCriteria;
import com.example.db.BaseEntity;
import com.example.dbutility.EntitySpecification;
import com.example.dbutility.ProductSpecification;
import com.example.utilities.PaginatedEntity;
@Service
public abstract class AbstractService <T extends BaseEntity,S> {

	//protected static final Logger logger = LoggerFactory.getLogger(AbstractService.class);
	protected static final Logger logger = LogManager.getLogger(AbstractService.class);

	@Autowired BaseSearchSpecification<BaseEntity, BaseEntity> baseSearchSpecification;
	
	PaginatedEntity<T> createPaginatedEntity(List<T> dataList,Integer pageNumber, Integer pageSize)
	{
		PagedListHolder<T> pages = new PagedListHolder<T>(dataList);
		pages.setPage(pageNumber); //set current page number
		pages.setPageSize(pageSize); // set the size of page
		pages.getPageList();
		//return pages.getPageList();
		
		PaginatedEntity<T> paginatedEntity=new PaginatedEntity<T>();
		
		paginatedEntity.setCurrentPage(pageNumber);
		paginatedEntity.setPageSize(pageSize);
		paginatedEntity.setNumberOfPages(pages.getPageCount());
		
		//paginatedEntity.setNumberOfItems(dataList.size());
		
		paginatedEntity.setHasNext(!pages.isLastPage());
		paginatedEntity.setHasPrevious(!pages.isFirstPage());
		paginatedEntity.setDataList(dataList);
		
		return paginatedEntity;
		
	}
	
	
	/*
	 * List<T> trial(Class<T> clazz,Class<P> joinedClazz) {
	 * 
	 * CriteriaBuilder builder = session.getCriteriaBuilder(); CriteriaQuery<T>
	 * criteriaQuery = builder.createQuery(clazz); Root<T> root =
	 * criteriaQuery.from(clazz);
	 * 
	 * Join<T, P> join = from.join("department", JoinType.INNER);
	 * criteriaQuery.where(builder.equal(join.get("specific_fied"),
	 * your_specific_fied));
	 * 
	 * TypedQuery<T> typedQuery = session.createQuery(criteriaQuery);
	 * 
	 * List<T> dataList = typedQuery.getResultList(); for (T t : dataList) {
	 * this.logger.info("Values::::::"+t.toString()); }
	 * 
	 * session.getTransaction().commit(); session.close();
	 * entityManagerFactory.close(); }
	 */
	
	@Bean
	protected ProductSpecification productSpecification() {
	    return new ProductSpecification();
	}
	
}
