package com.example.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.example.db.Product;
import com.example.dbutility.DatabaseConditions;
import com.example.dbutility.ProductSpecification;
import com.example.dbutility.SearchCriteria;
import com.example.repository.PaginatedProductRepository;
import com.example.repository.ProductDynamicPaginatedRepository;
import com.example.repository.ProductRepository;

@Service
public class ProductService extends AbstractService<Product,Integer> implements IProductService {

	@Autowired(required = true)
	private	ProductRepository productRepository;
	@Autowired
	private PaginatedProductRepository paginatedProductRepository;

	@Autowired
	private ProductDynamicPaginatedRepository productDynamicPaginatedRepository;

	
	@Override
	public List<Product> findAll() {
	
		return this.productRepository.findAll();
	
	}
	
	@Override
	public Product save(Product product) {
	
		return this.productRepository.save(product);
	
	}
	
	
	@Override
	public Product update(Product product) {
		
		return this.productRepository.save(product);
		
	}
	
	@Override
	public Product findById(int id) {
		Product product = new Product();

		product = this.productRepository.getById(id);

		return product;
	
	}
	
	@Override
	public List<Product> findByParam(Integer param) {
		List<Product> products= new ArrayList<Product>();		
		
		products=this.productRepository.findByParam(param);
		this.logger.info("Service Products:::"+products);
		
		return products;
	}

	
	public Page<Product> findByParamPaginated(DatabaseConditions databaseConditions)
	{
		return productDynamicPaginatedRepository.findAll(this.productSpecification().findByCriteria(databaseConditions.getSearchCriteria()),databaseConditions.getPageRequest());
				
	}

	
	
	

}
