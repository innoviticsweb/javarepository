package com.example.restservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.business.BusinessProduct;
import com.example.business.BusinessSearchCriteria;
import com.example.businessservice.ProductBusinessService;
import com.example.exceptions.BusinessException;
import com.example.exceptions.GeneralException;

@RestController
@RequestMapping("/api/product")
public class ProductController extends BaseGenericRestController<BusinessProduct,Integer> {

	@Autowired ProductBusinessService productBusinessService;
	@Override
	@GetMapping("/findAll")
	protected ResponseEntity<BaseGenericResponse<BusinessProduct>> list() {
		try
		{
			return this.generateBaseGenericResponse(BusinessProduct.class,null,this.productBusinessService.findAll(),null);
		}
		catch (BusinessException businessException) 
		{
			return this.handleBaseGenericResponseException(businessException);
		}
	}

	@Override
	@GetMapping("/findById")
	protected ResponseEntity<BaseGenericResponse<BusinessProduct>> read(@RequestParam (value= "id")Integer id) {
		try
		{
			return this.generateBaseGenericResponse(BusinessProduct.class,this.productBusinessService.findById(id),null,null);
		}
		catch (BusinessException businessException) {
			return this.handleBaseGenericResponseException(businessException);
		}
	}

	@Override
	@PostMapping(value = "/save",
	consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE},
	produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	protected ResponseEntity<BaseGenericResponse<BusinessProduct>> save(@RequestBody BusinessProduct entity) {
		try
		{
			return this.generateBaseGenericResponse(BusinessProduct.class,this.productBusinessService.save(entity),null,null);
		}
		catch (BusinessException businessException) {
			return this.handleBaseGenericResponseException(businessException);
		}
	}

	@Override
	@PostMapping(value = "/update",
	consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE},
	produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	protected ResponseEntity<BaseGenericResponse<BusinessProduct>> update(@RequestBody BusinessProduct entity) {
		try
		{
			return this.generateBaseGenericResponse(BusinessProduct.class,this.productBusinessService.update(entity),null,null);
		}
		catch (BusinessException businessException) {
			return this.handleBaseGenericResponseException(businessException);
		}
	}

	@Override
	@GetMapping("/findAllByParam")
	protected ResponseEntity<BaseGenericResponse<BusinessProduct>> listByParameter(@RequestParam (value= "id") Integer parentId) {
		try
		{
			return this.generateBaseGenericResponse(BusinessProduct.class,null,this.productBusinessService.findAllByParentId(parentId),null);
		}
		catch (BusinessException businessException) 
		{
			return this.handleBaseGenericResponseException(businessException);
		}
	}
	@Override
	@PostMapping(value = "/findAllByParamPaginated",
	consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE},
	produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	protected ResponseEntity<BaseGenericResponse<BusinessProduct>> listPaginatedByParameter(@RequestBody BusinessSearchCriteria businessSearchCriteria) throws BusinessException {
		try
		{
			return this.generateBaseGenericResponse(BusinessProduct.class,null,null,this.productBusinessService.findAllByParentIdPaginated(businessSearchCriteria));
		}
		catch (BusinessException businessException) 
		{
			return this.handleBaseGenericResponseException(businessException);
		}
	}

}
