package com.example.restservice;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.business.AuthenticationRequest;
import com.example.business.AuthenticationResponse;
import com.example.business.BusinessUser;
import com.example.business.BusinessSearchCriteria;
import com.example.businessservice.UserBusinessService;
import com.example.exceptions.BusinessException;
import com.example.security.JwtUtil;
import com.example.security.MyUserDetailsService;
import com.example.utilities.ErrorCode;
import com.example.utilities.StringUtility;


@RestController
@CrossOrigin
public class JwtAuthenticationController extends BaseGenericRestController<AuthenticationResponse, String> {

	//@Autowired AuthenticationManager  authenticationManager;
	//@Autowired MyUserDetailsService  userDetailsService;
	@Autowired UserBusinessService  userBusinessService;
	@Autowired JwtUtil  jwtUtil;
	
	@RequestMapping(value="/authenticate", method=RequestMethod.POST)
	public ResponseEntity<BaseGenericResponse<AuthenticationResponse>> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception, BusinessException
	{
		BusinessUser businessUser = new BusinessUser();
		try {
			this.logger.info("Authentication Request:::"+authenticationRequest.toString());
			if (authenticationRequest != null && StringUtility.isStringPopulated(authenticationRequest.getUsername())
					&& StringUtility.isStringPopulated(authenticationRequest.getPassword())) {
				//businessUser = this.userBusinessService.findByUsernameAndPassword(authenticationRequest.getUsername(),authenticationRequest.getPassword());
				businessUser=this.userBusinessService.findByUsernameAndPassword(authenticationRequest.getUsername(),authenticationRequest.getPassword());
				
			}

			else {
				throw new BusinessException(ErrorCode.BAD_CREDENTIALS);
			}

			final UserDetails userDetails = new User(businessUser.getName(), businessUser.getPassword(),
					new ArrayList<>());

			final String jwt = jwtUtil.generateToken(userDetails);

			return this.generateBaseGenericResponse(AuthenticationResponse.class, new AuthenticationResponse(jwt,businessUser.getBusinessRoles()),null,null);
			
		} 
		catch (BusinessException businessException) {
			return this.handleBaseGenericResponseException(businessException);
		}
	}

	@Override
	protected ResponseEntity<BaseGenericResponse<AuthenticationResponse>> list() throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ResponseEntity<BaseGenericResponse<AuthenticationResponse>> read(Integer id) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ResponseEntity<BaseGenericResponse<AuthenticationResponse>> save(AuthenticationResponse entity)
			throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ResponseEntity<BaseGenericResponse<AuthenticationResponse>> update(AuthenticationResponse entity)
			throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ResponseEntity<BaseGenericResponse<AuthenticationResponse>> listByParameter(String s)
			throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ResponseEntity<BaseGenericResponse<AuthenticationResponse>> listPaginatedByParameter(BusinessSearchCriteria businessSearchCriteria) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

}