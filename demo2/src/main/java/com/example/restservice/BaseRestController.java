package com.example.restservice;

import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.business.BaseBusinessEntity;
import com.example.db.BaseEntity;
import com.example.exceptions.GeneralException;
import com.example.properties.ConfigProperties;
import com.example.utilities.ErrorCode;
import com.example.utilities.ExceptionHandler;
import com.example.utilities.StringUtility;
import com.example.utilities.mappers.Mapper;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

abstract public class BaseRestController<T extends BaseBusinessEntity,S> {
	  
	@Autowired ExceptionHandler exceptionHandler;
	  public final static Logger logger = LogManager.getLogger(BaseRestController.class.getName());
	  static ConfigProperties configProperties = new ConfigProperties();

	  protected abstract ResponseEntity<BaseResponse<List<T>>> list() ;
	  
	  protected abstract ResponseEntity<BaseResponse<T>> read(Integer id);
	  
	  protected abstract ResponseEntity<BaseResponse<T>> save(T entity);
	  
	  protected abstract ResponseEntity<BaseResponse<T>> update(T entity);
	  
	  protected abstract ResponseEntity<BaseResponse<List<T>>> listByParameter(S s);
	  /*************************************************************************************************************************/
	  /*
	  protected abstract ResponseEntity<BaseGenericResponse<T>> list() ;
	  
	  protected abstract ResponseEntity<BaseGenericResponse<T>> read(Integer id);
	  
	  protected abstract ResponseEntity<BaseGenericResponse<T>> save(T entity);
	  
	  protected abstract ResponseEntity<BaseGenericResponse<T>> update(T entity);
	  
	  protected abstract ResponseEntity<BaseGenericResponse<T>> listByParameter(S s); 	  
	  */
	  
	  
	  
	  
	  
	  
		/* @Autowired ConfigProperties configProperties; */
		
		private HttpStatus validateSingleResponseSuccess (BaseResponse<T> baseResponse) 
		{
			if(baseResponse!=null&&checkIfErrorCodeInResponseIsSuccessful(baseResponse.getStatus())) 
			{ 
				return HttpStatus.OK; 
			}
			else return HttpStatus.INTERNAL_SERVER_ERROR;
		  
		}
		
		private HttpStatus validateListResponseSuccess (BaseResponse<List<T>> baseResponse) 
		{
			if(baseResponse!=null&&checkIfErrorCodeInResponseIsSuccessful(baseResponse.getStatus())) 
			{ 
				return HttpStatus.OK; 
			}
			else return HttpStatus.INTERNAL_SERVER_ERROR;
		  
		}
		
		protected ResponseEntity<BaseResponse<T>> generateSingleResponse(T data)
		{
			/* this.logger.info(data.toString()); */
			BaseResponse<T> response= new BaseResponse<T>(data);
			ResponseEntity<BaseResponse<T>> responseEntity = 
					new ResponseEntity<BaseResponse<T>>(this.constructSingleBaseResponseCodeAndMessage(response),this.validateSingleResponseSuccess(response));	  
			/* this.logger.info("Response:::"+responseEntity.toString()); */
			
			logger.info("Response::::"+responseEntity);
			return responseEntity; 
		}
		
		protected ResponseEntity<BaseResponse<List<T>>> generateListResponse(List<T> dataList)
		{
			BaseResponse<List<T>> response= new BaseResponse<List<T>>(dataList);
			ResponseEntity<BaseResponse<List<T>>> responseEntity = 
					new ResponseEntity<BaseResponse<List<T>>>(this.constructListBaseResponseCodeAndMessage(response),this.validateListResponseSuccess(response));	  
			/*
			 * ServerLog.info("Response:::"+responseEntity.toString(),StringUtility.
			 * getClassName(responseEntity));
			 */
			logger.info("Response::::"+responseEntity);
			logger.info("Path:::"+configProperties.getPath());
			return responseEntity; 
		}
		
		
		private  BaseResponse<T> constructSingleBaseResponseCodeAndMessage(BaseResponse<T> baseResponse)
		{
			baseResponse.setMessage(StringUtility.SUCCESS);
			baseResponse.setStatus(StringUtility.SUCCESS_CODE);
			return baseResponse;
		}

		private  BaseResponse<List<T>> constructListBaseResponseCodeAndMessage(BaseResponse<List<T>> baseResponse)
		{
			baseResponse.setMessage(StringUtility.SUCCESS);
			baseResponse.setStatus(StringUtility.SUCCESS_CODE);
			return baseResponse;
		}

		
		
		private boolean checkIfErrorCodeInResponseIsSuccessful(Integer errorCode)
		{
			return (errorCode!=null&&errorCode.intValue()==0);
		}
		
		
		 public BaseResponse<T> generateSingleFailureBaseResponse(int errorCode,String errorMessage)
		{
			 	BaseResponse<T> baseResponse= new BaseResponse<T>();
			 	baseResponse.setMessage(errorMessage);
				baseResponse.setStatus(errorCode);
				return baseResponse;
			
		}
		 
		 public BaseResponse<List<T>> generateListFailureBaseResponse(int errorCode,String errorMessage)
		{
			 	BaseResponse<List<T>> baseResponse= new BaseResponse<List<T>>();
			 	baseResponse.setMessage(errorMessage);
				baseResponse.setStatus(errorCode);
				return baseResponse;
				
		}
		 
		 protected ResponseEntity<BaseResponse<T>> generateFailureResponse(GeneralException exception,HttpStatus httpStatus)
			{
				
				ResponseEntity<BaseResponse<T>> responseEntity=new ResponseEntity<BaseResponse<T>>(this.generateSingleFailureBaseResponse(exception.getErrorCode(),exception.getErrorMessage()),httpStatus);
				/*
				 * this.logger.info("Response:::"+responseEntity.toString());
				 * this.logger.info("Response:::"+this.buildingEnvironment.getEnvironment().
				 * toString());
				 */
				
				return  responseEntity;
			}
			
			protected ResponseEntity<BaseResponse<List<T>>> generateFailureListResponse(GeneralException exception,HttpStatus httpStatus)
			{
				ResponseEntity<BaseResponse<List<T>>> responseEntity=new ResponseEntity<BaseResponse<List<T>>>(this.generateListFailureBaseResponse(exception.getErrorCode(),exception.getErrorMessage()),httpStatus);
				/* this.logger.info("Response:::"+responseEntity.toString()); */
				return responseEntity; 
			}
	  	

			
			protected ResponseEntity<BaseResponse<T>> handleException(GeneralException exception) {
				
				return this.generateFailureResponse(exception, HttpStatus.CONFLICT);
			}

			
			protected ResponseEntity<BaseResponse<List<T>>> handleListException(GeneralException exception) {
				
				return this.generateFailureListResponse(exception, HttpStatus.CONFLICT);
				
			}
			
}
