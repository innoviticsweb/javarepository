package com.example.restservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.business.BusinessUser;
import com.example.business.RestUserBusiness;
import com.example.business.BusinessSearchCriteria;
import com.example.businessservice.RestUserBusinessService;
import com.example.exceptions.BusinessException;
import com.example.exceptions.IntegrationException;
@RestController
@RequestMapping("/api/restuser")
public class RestUserController extends BaseGenericRestController<RestUserBusiness,String>  {

	@Autowired
	RestUserBusinessService restUserBusinessService;
	
	@Override	
	@GetMapping("/findAll") 
	protected ResponseEntity<BaseGenericResponse<RestUserBusiness>> list() throws BusinessException {
		try {
			return this.generateBaseGenericResponse(RestUserBusiness.class,null,this.restUserBusinessService.list(),null);
		} 
		
		catch (BusinessException businessException) 
		{
			return this.handleBaseGenericResponseException(businessException);
		}
	}

	@Override
	@GetMapping("/findById")
	protected ResponseEntity<BaseGenericResponse<RestUserBusiness>> read(@RequestParam Integer id) throws BusinessException {
		try {
			return this.generateBaseGenericResponse(RestUserBusiness.class,this.restUserBusinessService.getById(String.valueOf(id)),null,null);
		} 
		
		catch (BusinessException businessException) 
		{
			return this.handleBaseGenericResponseException(businessException);
		}
	}
	
	@PostMapping(value = "/save",
	consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE},
	produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	@Override
	protected ResponseEntity<BaseGenericResponse<RestUserBusiness>> save(@RequestBody RestUserBusiness entity)
			throws BusinessException, IntegrationException {
		
		
		
		try {
			return this.generateBaseGenericResponse(RestUserBusiness.class,this.restUserBusinessService.save(entity),null,null);
		} 
		
		catch (BusinessException businessException) 
		{
			return this.handleBaseGenericResponseException(businessException);
		}
	}

	@Override
	protected ResponseEntity<BaseGenericResponse<RestUserBusiness>> update(RestUserBusiness entity)
			throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ResponseEntity<BaseGenericResponse<RestUserBusiness>> listByParameter(String s) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ResponseEntity<BaseGenericResponse<RestUserBusiness>> listPaginatedByParameter(@RequestBody  BusinessSearchCriteria businessSearchCriteria) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

}
