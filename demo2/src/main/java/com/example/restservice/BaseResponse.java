package com.example.restservice;

public class BaseResponse <T> {
	protected Integer status;
	protected String message;
	protected  T data;
    
	public BaseResponse(/* Integer status, String message, */T data) {
		/*
		 * this.status = status; this.message = message;
		 */
        this.data = data;
    }
	public BaseResponse() {
		// TODO Auto-generated constructor stub
	}
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	@Override
	public String toString() {
		return "BaseResponse [status=" + status + ", message=" + message + ", data=" + data + "]";
	}
	
	
	
	
	
}
