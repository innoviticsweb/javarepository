package com.example.restservice;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.business.BaseBusinessEntity;
import com.example.business.BusinessSearchCriteria;
import com.example.exceptions.BusinessException;
import com.example.exceptions.GeneralException;
import com.example.exceptions.IntegrationException;
import com.example.utilities.CustomJsonRootName;
import com.example.utilities.ErrorCode;
import com.example.utilities.ListUtility;
import com.example.utilities.PaginatedEntity;
import com.example.utilities.StringUtility;

public abstract class BaseGenericRestController<T extends BaseBusinessEntity, S> {

	@Autowired ListUtility<T> listUtility;
	public final static Logger logger = LogManager.getLogger(BaseGenericRestController.class.getName());

	protected abstract ResponseEntity<BaseGenericResponse<T>> list()throws BusinessException;

	protected abstract ResponseEntity<BaseGenericResponse<T>> read(Integer id)throws BusinessException;

	protected abstract ResponseEntity<BaseGenericResponse<T>> save(T entity)throws BusinessException, IntegrationException;

	protected abstract ResponseEntity<BaseGenericResponse<T>> update(T entity)throws BusinessException;

	protected abstract ResponseEntity<BaseGenericResponse<T>> listByParameter(S s)throws BusinessException;
	
	protected abstract ResponseEntity<BaseGenericResponse<T>> listPaginatedByParameter(BusinessSearchCriteria businessSearchCriteria)throws BusinessException;

	private HttpStatus validateGenericResponseSuccess(BaseGenericResponse<T> baseGenericResponse) {
		if (baseGenericResponse != null
				&& checkIfErrorCodeInBaseGenericResponseIsSuccessful(baseGenericResponse.getStatus())) {
			return HttpStatus.OK;
		} else
			return HttpStatus.INTERNAL_SERVER_ERROR;

	}

	private BaseGenericResponse<T> constructGenericBaseResponseCodeAndMessage(BaseGenericResponse<T> baseResponse) {
		baseResponse.setMessage(StringUtility.SUCCESS);
		baseResponse.setStatus(StringUtility.SUCCESS_CODE);
		return baseResponse;
	}

	private boolean checkIfErrorCodeInBaseGenericResponseIsSuccessful(Integer errorCode) {
		return (errorCode != null && errorCode.intValue() == 0);
	}

	protected ResponseEntity<BaseGenericResponse<T>> handleBaseGenericResponseException(BusinessException businessException) {
		this.logger.info("Exception Caught::::");
		return this.generateFailureBaseGenericResponseEntity(businessException, this.determineErrorType(businessException.getErrorCode()));
	}

	private HttpStatus determineErrorType(int errorCode) {
		this.logger.info("Determine the Error Code:::"+errorCode);
		HttpStatus httpStatus=HttpStatus.BAD_REQUEST;
		if(errorCode==ErrorCode.NO_DATA_FOUND.getCode())
		{
			httpStatus= HttpStatus.BAD_REQUEST;
		}
		else if(errorCode==ErrorCode.OPERATION_NOT_PERFORMED.getCode())
		{
			httpStatus=HttpStatus.INTERNAL_SERVER_ERROR;
		}
				return httpStatus;
	}

	protected ResponseEntity<BaseGenericResponse<T>> generateFailureBaseGenericResponseEntity(
			GeneralException exception, HttpStatus httpStatus) {
		
		ResponseEntity<BaseGenericResponse<T>> responseEntity = new ResponseEntity<BaseGenericResponse<T>>(
				this.generateBaseGenericResponseFailure(exception.getErrorCode(), exception.getErrorMessage()),
				httpStatus);
		return responseEntity;
	}

	public BaseGenericResponse<T> generateBaseGenericResponseFailure(int errorCode, String errorMessage) {
		this.logger.info("Generating the Base Response Failure using :::"+errorCode+" and " +errorMessage);
		BaseGenericResponse<T> baseGenericResponse = new BaseGenericResponse<T>();
		baseGenericResponse.setMessage(errorMessage);
		baseGenericResponse.setStatus(errorCode);
		return baseGenericResponse;

	}

	protected ResponseEntity<BaseGenericResponse<T>> generateBaseGenericResponse(Class<T> clazz, T data,
			List<T> dataList,PaginatedEntity<T> paginatedList) throws BusinessException {
		
		BaseGenericResponse<T> baseGenericResponse = new BaseGenericResponse<T>();
		String singleAnnotation=clazz.getAnnotation(CustomJsonRootName.class).singular();
		String pluralAnnotation=clazz.getAnnotation(CustomJsonRootName.class).plural();
		
		if (data != null) {
			Map<String, T> object=new HashMap<String, T>();
			object.put(singleAnnotation, data);
			//baseGenericResponse.setObject(object);
			baseGenericResponse.setResponse(object);
			
			
		} else if (listUtility.isListPopulated(dataList)) {
			Map<String, List<T>> result = new HashMap<String, List<T>>();
			result.put(pluralAnnotation, dataList);
			//baseGenericResponse.setResult(result);
			baseGenericResponse.setResponse(result);
			
		}
		else if (this.listUtility.isPaginatedListPopulated(paginatedList)) 
		{
			
			Map<String, PaginatedEntity<T>> page=new HashMap<String, PaginatedEntity<T>>();
			page.put(pluralAnnotation, paginatedList);
			baseGenericResponse.setResponse(page);
			
			/*
			Map<String, List<T>> result = new HashMap<String, List<T>>();
			result.put(pluralAnnotation, paginatedList.getDataList());
			baseGenericResponse.setHasNext(paginatedList.getHasNext());
			baseGenericResponse.setHasPrevious(paginatedList.getHasPrevious());
			baseGenericResponse.setPageSize(paginatedList.getPageSize());
			baseGenericResponse.setNumberOfPages(paginatedList.getNumberOfPages());
			baseGenericResponse.setNumberOfItems(paginatedList.getNumberOfItems());
			*/
			
		}
		
		
		else if(data==null&&(listUtility.isListEmptyOrNull(dataList))&&(listUtility.isPaginatedListEmpty(paginatedList)))
		{
			this.logger.info("No data found:::" + baseGenericResponse.toString());
			this.logger.info("Base Response:::" + baseGenericResponse.toString());
			throw new BusinessException(ErrorCode.NO_DATA_FOUND);
		}
		
		this.logger.info("Base Response:::" + baseGenericResponse.toString());

		this.logger.info("Data found:::" + baseGenericResponse.toString());
		return new ResponseEntity<BaseGenericResponse<T>>(
				this.constructGenericBaseResponseCodeAndMessage(baseGenericResponse),
				this.validateGenericResponseSuccess(baseGenericResponse));

	}

}
