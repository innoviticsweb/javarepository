package com.example.restservice;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.business.BusinessUser;
import com.example.business.BusinessSearchCriteria;
import com.example.businessservice.UserBusinessService;
import com.example.db.User;
import com.example.exceptions.BusinessException;
import com.example.exceptions.GeneralException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

@RestController
@RequestMapping("/api/user")
public class UserController extends BaseGenericRestController<BusinessUser,String> {
	@Autowired(required = true)
	private UserBusinessService userBusinessService;
	
	

	@Override
	@GetMapping("/findById") 
	protected @ResponseBody ResponseEntity<BaseGenericResponse<BusinessUser>> read(@RequestParam (value= "id") Integer id) {
		try 
		{
		
			//return this.generateBaseGenericResponse(this.userBusinessService.findById(id),null);
			return this.generateBaseGenericResponse(BusinessUser.class,this.userBusinessService.findById(id),null ,null);
		
		}
		catch (BusinessException businessException) 
		{
			return this.handleBaseGenericResponseException(businessException);
		}
		
	}

	@Override
	@GetMapping("/findAll")
	protected @ResponseBody ResponseEntity<BaseGenericResponse<BusinessUser>> list() {
		try 
		{
			return this.generateBaseGenericResponse(BusinessUser.class,null,this.userBusinessService.findAll(),null);	
		}
		catch(BusinessException businessException)
		{
			return this.handleBaseGenericResponseException(businessException);
		}
				 
	}
	
	@Override
	@GetMapping("/findAllByPassword")
	protected @ResponseBody ResponseEntity<BaseGenericResponse<BusinessUser>> listByParameter(@RequestParam (value= "password") String password) {
		try 
		{
			return this.generateBaseGenericResponse(BusinessUser.class,null,this.userBusinessService.findAllByPassword(password),null);	
		}
		catch(BusinessException businessException)
		{
			return this.handleBaseGenericResponseException(businessException);
		}
				 
	}
	
	
	@Override
	@PostMapping(value = "/save",
	consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE},
	produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	protected ResponseEntity<BaseGenericResponse<BusinessUser>> save(@RequestBody BusinessUser entity) {
		
		try {
			return this.generateBaseGenericResponse(BusinessUser.class,this.userBusinessService.save(entity),null,null);
		} 
		
		catch (BusinessException businessException) 
		{
			return this.handleBaseGenericResponseException(businessException);
		}
		
	}

	@Override
	@PostMapping(value = "/update",
	consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE},
	produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	
	protected ResponseEntity<BaseGenericResponse<BusinessUser>>update(@RequestBody BusinessUser entity) {
		try
		{
		return this.generateBaseGenericResponse(BusinessUser.class,this.userBusinessService.update(entity),null,null);
		}
		catch(BusinessException businessException)
		{
			return this.handleBaseGenericResponseException(businessException);
		}
	}


	/*
	  @GetMapping("/findByIdMapped") protected @ResponseBody
	  ResponseEntity<BaseStringResponse> findByIdMapped(@RequestParam (value= "id")
	  Integer id) { try { return
	  this.generatedStringMappedObjectList("BusinessUserss",null,this.
	  userBusinessService.findById(id)); } catch(GeneralException generalException)
	  { return this.handleBaseStringResponseException(generalException); } }
	  */
	@GetMapping("/findAllMapped") 
	
	protected @ResponseBody ResponseEntity<BaseGenericResponse<BusinessUser>> listMapped() {
		try {
			return this.generateBaseGenericResponse(BusinessUser.class,null, this.userBusinessService.findAll(),null);
		} catch (BusinessException businessException) {
			return this.handleBaseGenericResponseException(businessException);
		}

	}

	@Override
	protected ResponseEntity<BaseGenericResponse<BusinessUser>> listPaginatedByParameter(@RequestParam BusinessSearchCriteria businessSearchCriteria) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}
	
}