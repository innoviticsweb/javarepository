package com.example.security;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.business.BusinessUser;
import com.example.businessservice.UserBusinessService;
import com.example.exceptions.BusinessException;
import com.example.utilities.ErrorCode;
@Service
public class MyUserDetailsService implements UserDetailsService {

	@Autowired
	UserBusinessService userBusinessService;
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		BusinessUser businessUser=new BusinessUser();
		try {
			businessUser = this.userBusinessService.findByName(username);
		} catch (BusinessException e) {
		
			e.printStackTrace();
		}
				
		return new User(businessUser.getName(),businessUser.getPassword(),new ArrayList<>());
	}

}
