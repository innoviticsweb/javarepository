package com.example.demo2;



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.example.properties.ConfigProperties;


@SpringBootApplication
@ComponentScan(basePackages = {"com.example.restservice"})
@ComponentScan(basePackages = {"com.example.service"})
@ComponentScan(basePackages = {"com.example.businessservice"})
@ComponentScan(basePackages = {"com.example.repository"})
@ComponentScan(basePackages = {"com.example.db"})
@ComponentScan(basePackages = {"com.example.restapiconsumer"})
@ComponentScan(basePackages = {"com.example.business"})
@ComponentScan(basePackages = {"com.example.exceptions"})
@ComponentScan(basePackages = {"com.example.utilities"})
@ComponentScan(basePackages= {"com.example.restentities"})
@ComponentScan(basePackages= {"com.example.utilities.security"})
@ComponentScan(basePackages= {"com.example.security"})
@ComponentScan(basePackages= {"com.example.restutilities"})
@ComponentScan(basePackages= {"com.example.restapiconsumer"})
@ComponentScan(basePackages= {"com.example.dbutilities"})

@EntityScan("com.example.db")
@EnableJpaRepositories(basePackages = {"com.example.repository"})
@ConfigurationPropertiesScan(basePackages = {"com.example.properties"})

@EnableConfigurationProperties(ConfigProperties.class) 

@EnableJpaAuditing


public class Demo2Application extends SpringBootServletInitializer{
	
	public static void main(String[] args) {
		SpringApplication.run(Demo2Application.class, args);
	}
	 @Override
	  protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
	    return builder.sources(Demo2Application.class);
	  }
}
