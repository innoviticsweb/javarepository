package com.example.business;

import com.example.utilities.CustomJsonRootName;
import com.fasterxml.jackson.annotation.JsonCreator;

@CustomJsonRootName(plural = "restUsers", singular = "restUser")
public class RestUserBusiness extends BaseBusinessEntity{

	private int id;
    private String name;
    private String email;
    private String gender;
    private String status;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@JsonCreator
	public RestUserBusiness(int id, String name, String email, String gender, String status) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.gender = gender;
		this.status = status;
	}
	public RestUserBusiness() {
		super();
	}
	@Override
	public String toString() {
		return "RestUserBusiness [id=" + id + ", name=" + name + ", email=" + email + ", gender=" + gender + ", status="
				+ status + "]";
	}
    
    
}
