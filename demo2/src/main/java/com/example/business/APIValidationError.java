package com.example.business;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
public class APIValidationError {

	private String errorCode;
    private String message;
 
    public APIValidationError(String errorCode, String message) {
        super();
        this.errorCode = errorCode;
        this.message = message;
    }
}
