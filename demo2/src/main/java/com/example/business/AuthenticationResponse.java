package com.example.business;

import java.util.List;

import com.example.utilities.CustomJsonRootName;

@CustomJsonRootName(plural = "credentials", singular = "credentials")
public class AuthenticationResponse extends BaseBusinessEntity {



		private final String token;
		
		private List<BusinessRole> roles;
		
		public String getToken() {
			return token;
		}

		public AuthenticationResponse(String token) {
			this.token = token;
		}

		
		
		
		public List<BusinessRole> getRoles() {
			return roles;
		}

		public void setRoles(List<BusinessRole> roles) {
			this.roles = roles;
		}

		public AuthenticationResponse(String token, List<BusinessRole> roles) {
			super();
			this.token = token;
			this.roles = roles;
		}

		@Override
		public String toString() {
			return "AuthenticationResponse [token=" + token + ", businessRoles=" + roles + "]";
		}

		
}
