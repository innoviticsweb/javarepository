package com.example.business;

import com.example.utilities.CustomJsonRootName;

@CustomJsonRootName(plural = "attributeFamilies", singular = "attributeFamily")
public class BusinessAttributeFamily extends BaseBusinessEntity{
	
	private Integer id;
	private String code;
	private String name;
	private String status;
	private boolean isUserDefined;
	private Integer rate;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public boolean isUserDefined() {
		return isUserDefined;
	}
	public void setUserDefined(boolean isUserDefined) {
		this.isUserDefined = isUserDefined;
	}
	public Integer getRate() {
		return rate;
	}
	public void setRate(Integer rate) {
		this.rate = rate;
	}
	@Override
	public String toString() {
		return "BusinessAttributeFamily [id=" + id + ", code=" + code + ", name=" + name + ", status=" + status
				+ ", isUserDefined=" + isUserDefined + ", rate=" + rate + "]";
	}

}
