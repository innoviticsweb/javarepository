package com.example.business;

import java.util.Date;

import com.example.utilities.CustomJsonRootName;
import com.fasterxml.jackson.annotation.JsonProperty;
@CustomJsonRootName(plural = "products", singular = "product")
public class BusinessProduct extends BaseBusinessEntity{
	private int id;
	private String sku;
	private String type;
	private Date createdAt;
	private Date updatedAt;
	private Integer parentId;
	@JsonProperty("attributeFamilies")
	private BusinessAttributeFamily businessAttributeFamily;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	public BusinessAttributeFamily getBusinessAttributeFamily() {
		return businessAttributeFamily;
	}
	public void setBusinessAttributeFamily(BusinessAttributeFamily businessAttributeFamily) {
		this.businessAttributeFamily = businessAttributeFamily;
	}
	@Override
	public String toString() {
		return "BusinessProduct [id=" + id + ", sku=" + sku + ", type=" + type + ", createdAt=" + createdAt
				+ ", updatedAt=" + updatedAt + ", parentId=" + parentId + ", businessAttributeFamily="
				+ businessAttributeFamily + "]";
	}
	
	
}
