package com.example.business;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.example.utilities.CustomJsonRootName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
@CustomJsonRootName(plural = "users", singular = "user")
//@JsonTypeName("BusinessUser")
public class BusinessUser extends BaseBusinessEntity{

	private int	id;
	private String 	name;
	private String email;
	private String password;
	private String rememberToken;
	private Date   createdAt;
	private Date	updatedAt;
	@JsonProperty("roles")
	private List<BusinessRole> businessRoles;
 
	@JsonCreator
	public BusinessUser(int id, String name, String email, String password, String rememberToken, Date createdAt,
			Date updatedAt) {
		
		this.id = id;
		this.name = name;
		this.email = email;
		this.password = password;
		this.rememberToken = rememberToken;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}
	
	public BusinessUser()
	{
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRememberToken() {
		return rememberToken;
	}
	public void setRememberToken(String rememberToken) {
		this.rememberToken = rememberToken;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public List<BusinessRole> getBusinessRoles() {
		return businessRoles;
	}

	public void setBusinessRoles(List<BusinessRole> businessRoles) {
		this.businessRoles = businessRoles;
	}

	@Override
	public String toString() {
		return "BusinessUser [id=" + id + ", name=" + name + ", email=" + email + ", password=" + password
				+ ", rememberToken=" + rememberToken + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt
				+ ", businessRoles=" + businessRoles + "]";
	}

	
	
	
	
}
