package com.example.business;

import java.util.Date;

import com.example.utilities.CustomJsonRootName;
@CustomJsonRootName(plural = "roles", singular = "role")
public class BusinessRole extends  BaseBusinessEntity {

	  private Integer id;
	  private String name;
	  private String  description;
	  private String  permissionType;
	  private String  permissions;
	  private Date createdAt;
	  private Date updatedAt;
	  
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPermissionType() {
		return permissionType;
	}
	public void setPermissionType(String permissionType) {
		this.permissionType = permissionType;
	}
	public String getPermissions() {
		return permissions;
	}
	public void setPermissions(String permissions) {
		this.permissions = permissions;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "BusinessRole [id=" + id + ", name=" + name + ", description=" + description + ", permissionType="
				+ permissionType + ", permissions=" + permissions + ", createdAt=" + createdAt + ", updatedAt="
				+ updatedAt + "]";
	}
	


	
}
