package com.example.db;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
@Table(name="products")

public class Product extends BaseEntity{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String sku;
	private String type;
	private Date createdAt;
	private Date updatedAt;
	private Integer parentId;
	@ManyToOne
	@JoinColumn(name="ATTRIBUTE_FAMILY_ID")
	private AttributeFamily attributeFamily;

	/* private Integer attributeFamilyId; */
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	/*
	 * public Integer getAttributeFamilyId() { return attributeFamilyId; } public
	 * void setAttributeFamilyId(Integer attributeFamilyId) { this.attributeFamilyId
	 * = attributeFamilyId; }
	 */
	public AttributeFamily getAttributeFamily() {
		return attributeFamily;
	}
	public void setAttributeFamily(AttributeFamily attributeFamily) {
		this.attributeFamily = attributeFamily;
	}
	@Override
	public String toString() {
		return "Product [id=" + id + ", sku=" + sku + ", type=" + type + ", createdAt=" + createdAt + ", updatedAt="
				+ updatedAt + ", parentId=" + parentId + ", attributeFamily=" + attributeFamily + "]";
	}
	
	
	
	

}
