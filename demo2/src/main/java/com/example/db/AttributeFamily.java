package com.example.db;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
@Table(name="attribute_families")
public class AttributeFamily extends BaseEntity{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	private String code;
	private String name;
	private String status;
	private boolean isUserDefined;
	private Integer rate;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public boolean isUserDefined() {
		return isUserDefined;
	}
	public void setUserDefined(boolean isUserDefined) {
		this.isUserDefined = isUserDefined;
	}
	public Integer getRate() {
		return rate;
	}
	public void setRate(Integer rate) {
		this.rate = rate;
	}
	@Override
	public String toString() {
		return "AttributeFamily [id=" + id + ", code=" + code + ", name=" + name + ", status=" + status
				+ ", isUserDefined=" + isUserDefined + ", rate=" + rate + "]";
	}
	
	
	
}
