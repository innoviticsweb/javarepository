package com.example.db;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
@Table(name="users")
public class User extends BaseEntity {
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
	
private int	id;
private String 	name;
private String email;
private String password;
private String rememberToken;
private Date   createdAt;
private Date	updatedAt;


  @ManyToMany(fetch=FetchType.EAGER,cascade = {CascadeType.ALL})
  
  @JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id"),
  inverseJoinColumns = @JoinColumn(name = "role_id")) Set<Role> roles = new HashSet<>();





public User() 
	{
	
	}

public User(int id, String name, String email, String password, String rememberToken, Date createdAt, Date updatedAt) {
	this.id = id;
	this.name = name;
	this.email = email;
	this.password = password;
	this.rememberToken = rememberToken;
	this.createdAt = createdAt;
	this.updatedAt = updatedAt;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getRememberToken() {
	return rememberToken;
}
public void setRememberToken(String rememberToken) {
	this.rememberToken = rememberToken;
}
public Date getCreatedAt() {
	return createdAt;
}
public void setCreatedAt(Date createdAt) {
	this.createdAt = createdAt;
}
public Date getUpdatedAt() {
	return updatedAt;
}
public void setUpdatedAt(Date updatedAt) {
	this.updatedAt = updatedAt;
}


  public Set<Role> getRoles() { return roles; }
  
  public void setRoles(Set<Role> roles) { this.roles = roles; }
 

@Override
public String toString() {
	return "User [id=" + id + ", name=" + name + ", email=" + email + ", password=" + password + ", rememberToken="
			+ rememberToken + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + ", roles=" + roles + "]";
}



}
