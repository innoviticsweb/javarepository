package com.example.restapiconsumer.user;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.exceptions.IntegrationException;
import com.example.restentities.external.GetRestUserRequest;
import com.example.restentities.external.SingleGetRestUserResponse;
import com.example.restentities.internal.GetRestUserInput;
import com.example.restentities.internal.GetRestUserOutput;
import com.example.restentities.internal.SingleRestUserOutput;
import com.example.utilities.ErrorCode;
import com.example.utilities.StringUtility;
@Service
public class GetSingleRestUserApiConsumer extends RestUserApiConsumer<GetRestUserRequest, SingleGetRestUserResponse, GetRestUserInput, GetRestUserOutput> {

	@Override
	public HttpEntity<GetRestUserRequest> generateRequestFromInput(GetRestUserInput input) {
		return new HttpEntity<>(this.generateHeaders());
	}

	@Override
	public GetRestUserOutput generateOutPutFromResponse(ResponseEntity<SingleGetRestUserResponse> responseEntity) {
		
		SingleRestUserOutput singleRestUserOutput=new SingleRestUserOutput();
		GetRestUserOutput getRestUserOutput=new GetRestUserOutput();
		
		singleRestUserOutput.setId(responseEntity.getBody().getId());
		singleRestUserOutput.setEmail(responseEntity.getBody().getEmail());
		singleRestUserOutput.setGender(responseEntity.getBody().getGender());
		singleRestUserOutput.setName(responseEntity.getBody().getName());
		singleRestUserOutput.setStatus(responseEntity.getBody().getStatus());
		getRestUserOutput.setRestUserOutput(singleRestUserOutput);
		
		return getRestUserOutput;
	}

	@Override
	public void validateResponse(ResponseEntity<SingleGetRestUserResponse> responseEntity) throws IntegrationException {
		if(!this.validateResponseStatus(responseEntity))
		{
			throw new IntegrationException(ErrorCode.FAILED_TO_INTEGRATE);
		}
	}

	@Override
	public IntegrationException handleException(Exception exception) {
		return this.exceptionHandler.handleAsIntegrationException(exception, ErrorCode.FAILED_TO_INTEGRATE);
	}

	@Override
	public String generateURL(String params) {
		return this.configProperties.getRestUserUrl()+"/users" + (StringUtility.isStringPopulated(params)?"/"+params:"");
	}

	@Override
	public HttpMethod chooseHttpMethod() {
		return  HttpMethod.GET;
	}

}
