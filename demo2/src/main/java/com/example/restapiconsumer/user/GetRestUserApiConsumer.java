package com.example.restapiconsumer.user;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.exceptions.IntegrationException;
import com.example.restentities.external.GetRestUserRequest;
import com.example.restentities.external.SingleGetRestUserResponse;
import com.example.restentities.internal.GetRestUserInput;
import com.example.restentities.internal.GetRestUserOutput;
import com.example.restentities.internal.SingleRestUserOutput;
import com.example.utilities.ErrorCode;
import com.example.utilities.StringUtility;
@Service
public class GetRestUserApiConsumer extends RestUserApiConsumer<GetRestUserRequest, SingleGetRestUserResponse[], GetRestUserInput, GetRestUserOutput> {
	
	@Override
	public HttpEntity<GetRestUserRequest> generateRequestFromInput(GetRestUserInput input) {
		//return new HttpEntity<GetRestUserRequest>(null, this.generateHeaders());
		return new HttpEntity<>(this.generateHeaders());
	}

	@Override
	public GetRestUserOutput generateOutPutFromResponse(ResponseEntity<SingleGetRestUserResponse[]> responseEntity) {
		GetRestUserOutput output=new GetRestUserOutput();
		List<SingleRestUserOutput> restUserOutputs=new ArrayList<SingleRestUserOutput>();
		
		if(this.validateResponseStatus(responseEntity)&&responseEntity.hasBody())
		{		
				for(int i=0;i<responseEntity.getBody().length;i++)
				{
					SingleRestUserOutput singleRestUserOutput = new SingleRestUserOutput();
					singleRestUserOutput.setId(responseEntity.getBody()[i].getId());
					singleRestUserOutput.setName(responseEntity.getBody()[i].getName());
					singleRestUserOutput.setEmail(responseEntity.getBody()[i].getEmail());
					singleRestUserOutput.setStatus(responseEntity.getBody()[i].getStatus());
					singleRestUserOutput.setGender(responseEntity.getBody()[i].getGender());
					restUserOutputs.add(singleRestUserOutput);
				}
				output.setRestUserOutputs(restUserOutputs);
		}
	
		return output;
	}

	@Override
	public void validateResponse(ResponseEntity<SingleGetRestUserResponse[]> responseEntity) throws IntegrationException {
		
		if (!this.validateResponseStatus(responseEntity)) {
			throw new IntegrationException(ErrorCode.FAILED_TO_INTEGRATE);
		}
		if (responseEntity.getBody() == null) {
			throw new IntegrationException(ErrorCode.NO_DATA_FOUND);
		} 

	}

	@Override
	public IntegrationException handleException(Exception exception) {
		return this.exceptionHandler.handleAsIntegrationException(exception, ErrorCode.FAILED_TO_INTEGRATE);
	}

	@Override
	public String generateURL(String params) {
		return this.configProperties.getRestUserUrl()+"/users" + (StringUtility.isStringPopulated(params)?"/"+params:"");
	}

	@Override
	public HttpMethod chooseHttpMethod() {
		return HttpMethod.GET;
	}

	

}
