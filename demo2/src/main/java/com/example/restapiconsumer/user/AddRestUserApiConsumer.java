package com.example.restapiconsumer.user;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.exceptions.IntegrationException;
import com.example.restentities.external.AddRestUserRequest;
import com.example.restentities.external.AddRestUserResponse;
import com.example.restentities.internal.AddRestUserInput;
import com.example.restentities.internal.AddRestUserOutput;
import com.example.utilities.ErrorCode;
@Service
public class AddRestUserApiConsumer extends RestUserApiConsumer <AddRestUserRequest,AddRestUserResponse,AddRestUserInput,AddRestUserOutput>{

	@Override
	public IntegrationException handleException(Exception exception) {
		return this.exceptionHandler.handleAsIntegrationException(exception, ErrorCode.FAILED_TO_INTEGRATE);
	}

	@Override
	public HttpEntity<AddRestUserRequest> generateRequestFromInput(AddRestUserInput input) {
		AddRestUserRequest addRestUserRequest=new AddRestUserRequest();
		
		if(input!=null)
		{
			addRestUserRequest.setId(input.getId());
			addRestUserRequest.setEmail(input.getEmail());
			addRestUserRequest.setGender(input.getGender());
			addRestUserRequest.setName(input.getName());
			addRestUserRequest.setStatus(input.getStatus());
		}
		
		HttpEntity<AddRestUserRequest> httpEntity=new HttpEntity<AddRestUserRequest>(addRestUserRequest, this.generateHeaders());
		return httpEntity;
	}

	@Override
	public AddRestUserOutput generateOutPutFromResponse(ResponseEntity<AddRestUserResponse> responseEntity) {
		AddRestUserOutput addRestUserOutput= new AddRestUserOutput();
		if (this.validateResponseStatus(responseEntity)&&responseEntity.hasBody())
		{
			AddRestUserResponse addRestUserResponse=responseEntity.getBody();
			
			addRestUserOutput.setId(addRestUserResponse.getId());
			addRestUserOutput.setEmail(addRestUserResponse.getEmail());
			addRestUserOutput.setGender(addRestUserResponse.getGender());
			addRestUserOutput.setName(addRestUserResponse.getName());
			addRestUserOutput.setStatus(addRestUserResponse.getStatus());
		}
		
		return addRestUserOutput;
	}

	
	@Override
	public void validateResponse(ResponseEntity<AddRestUserResponse> responseEntity) throws IntegrationException {
		if(!this.validateResponseStatus(responseEntity))
		{
			throw new IntegrationException(ErrorCode.FAILED_TO_INTEGRATE);
		}
		if (responseEntity.getBody()==null)
		{
			throw new IntegrationException(ErrorCode.NO_DATA_FOUND);
		}
		
	}

	@Override
	public String generateURL(String params) {
		return this.configProperties.getRestUserUrl()+"/users";
		
	}

	@Override
	public HttpMethod chooseHttpMethod() {
		return HttpMethod.POST;
	}

	
}
