package com.example.restapiconsumer.user;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.example.properties.MyConfig;
import com.example.properties.RestUserConfiguration;
import com.example.restapiconsumer.AbstractBaseRestConsumer;

@Service
public abstract class RestUserApiConsumer<RestUserRequest, RestUserResponse, RestUserInput, RestUserOutput> extends AbstractBaseRestConsumer<RestUserRequest, RestUserResponse, RestUserInput, RestUserOutput>
{	
	
	@Override
	public HttpHeaders generateHeaders() {
		HttpHeaders headers=this.httpHeaders();
		headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBearerAuth(this.configProperties.getRestUserToken());
		logger.info("Generated Headers:::"+headers.toString());
		return headers;
	}

}
