package com.example.restapiconsumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.exceptions.IntegrationException;
import com.example.properties.ConfigProperties;
import com.example.utilities.ExceptionHandler;
@Service
public abstract class AbstractBaseRestConsumer<REQ,RES,I,O>
//<BaseRequest,BaseResponse,BaseInput,BaseOutput> 
implements BaseRestConsumer<REQ,RES,I,O> {

	@Autowired protected ConfigProperties configProperties;
//	@Autowired protected RestTemplate restTemplate;
//	@Autowired protected HttpHeaders headers;
	@Autowired protected ExceptionHandler exceptionHandler;
	

	public O invoke(I input,Class<RES> clazz,String params) throws IntegrationException {
		logger.info("Input::" + input);
		
		try {

			HttpEntity<REQ> httpEntity= this.generateRequestFromInput(input);
			if(httpEntity!=null)
			{
				logger.info("Request::" + httpEntity.toString());
			}
			String url=this.generateURL(params);
			
			logger.info("URL:::"+url);
			
			//ResponseEntity<RES> responseEntity=this.consumeRestAPI(httpEntity, this.chooseHttpMethod(), );
			ResponseEntity<RES> responseEntity=this.consumeRestAPI(httpEntity, this.chooseHttpMethod(), clazz,params);
			
			
			
			
			logger.info("Response::" + responseEntity.toString());
			
			this.validateResponse(responseEntity);
			
			O output=this.generateOutPutFromResponse(responseEntity);
			
			return output;
			
		} catch (Exception exception) {
			throw this.handleException(exception);
		}
	}
	
	
	
	
	protected boolean validateResponseStatus(ResponseEntity<RES> responseEntity) 
	{	boolean result=false;
		
		boolean responseResult=responseEntity!=null&&responseEntity.getStatusCode()!=null&&responseEntity.getStatusCode().is2xxSuccessful();
		
		return responseResult;
	}
	
	@Override
	public
	ResponseEntity<RES> consumeRestAPI(HttpEntity<REQ> httpEntity,HttpMethod httpMethod,Class<RES> clazz,String params) throws Exception
	{						
		ResponseEntity<RES> responseEntity=this.restTemplate().exchange(this.generateURL(params), httpMethod, httpEntity, clazz);
		return responseEntity;
			
	}
	
	@Bean
	RestTemplate restTemplate() {
	    return new RestTemplate();
	}
	
	@Bean
	protected HttpHeaders httpHeaders() {
	    return new HttpHeaders();
	}
	
	
	
}
