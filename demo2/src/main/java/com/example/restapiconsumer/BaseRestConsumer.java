package com.example.restapiconsumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import com.example.exceptions.IntegrationException;

	public  interface BaseRestConsumer <REQ , RES ,I ,O > {
	
	public final static Logger logger = LogManager.getLogger(BaseRestConsumer.class.getName());
	
	HttpEntity<REQ> generateRequestFromInput(I input);
	
	O generateOutPutFromResponse(ResponseEntity<RES> responseEntity);
	
	ResponseEntity<RES> consumeRestAPI(HttpEntity<REQ> httpEntity,HttpMethod httpMethod,Class<RES> clazz,String params) throws Exception;
	
	void validateResponse(ResponseEntity<RES> responseEntity) throws IntegrationException;
	
	HttpHeaders generateHeaders();
	
	IntegrationException handleException(Exception exception);
		
	String generateURL(String params);
	
	HttpMethod chooseHttpMethod();
	
}
