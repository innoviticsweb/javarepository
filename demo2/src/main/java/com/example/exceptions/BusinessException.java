package com.example.exceptions;

import java.util.Arrays;
import java.util.Date;

import com.example.utilities.ErrorCode;

public class BusinessException extends GeneralException {

	private static final long serialVersionUID = 1L;

	public BusinessException(Integer errorCode, Date timestamp, String message, String description, StackTraceElement[] stackTrace) 
	{
		super(errorCode, timestamp, message, description, stackTrace);
		
	}
	
	public BusinessException(ErrorCode errorCode) 
	{
		super(errorCode);
		
	}

	@Override
	public String toString() {
		return "BusinessException [error=" + error + ", errorCode=" + errorCode + ", timestamp=" + timestamp
				+ ", errorMessage=" + errorMessage + ", description=" + description + "]";
	}



	/*
	 * *
	 * 
	 * 
	 */
	

}
