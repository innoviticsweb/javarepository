package com.example.exceptions;

import java.util.Arrays;
import java.util.Date;

import com.example.utilities.ErrorCode;

public class IntegrationException extends GeneralException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IntegrationException(int statusCode, Date timestamp, String message, String description, StackTraceElement[] stackTrace) {
		super(statusCode, timestamp, message, description, stackTrace);
	}

	
	public IntegrationException(ErrorCode errorCode) 
	{
		super(errorCode);
		
	}


	@Override
	public String toString() {
		return "IntegrationException [error=" + error + ", errorCode=" + errorCode + ", timestamp=" + timestamp
				+ ", errorMessage=" + errorMessage + ", description=" + description + "]";
	}


	
	
	
	
}
