package com.example.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
@Component
@Configuration
@ConstructorBinding
@ConfigurationProperties
public class ConfigProperties {
    
	private  String path;
	

	public  String getPath() 
	{
			return path;
	}


	
	 public void setPath(String path) { this.path = path; }
	    @Value("${restUser.api.url}")
	    private String restUserUrl;


	    @Value("${restUser.api.token}")
	    private String restUserToken;


		public String getRestUserUrl() {
			return restUserUrl;
		}


		public void setRestUserUrl(String restUserUrl) {
			this.restUserUrl = restUserUrl;
		}


		public String getRestUserToken() {
			return restUserToken;
		}


		public void setRestUserToken(String restUserToken) {
			this.restUserToken = restUserToken;
		}
	
	 
	 
	 
	
}