package com.example.properties;

import org.springframework.stereotype.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.context.annotation.Configuration;
@Configuration
@ConstructorBinding
@Component
public class MyConfig {

    @Value("${restUser.api.url}")
    private String restUserUrl;


    @Value("${restUser.api.token}")
    private String restUserToken;


	public String getRestUserUrl() {
		return restUserUrl;
	}


	public void setRestUserUrl(String restUserUrl) {
		this.restUserUrl = restUserUrl;
	}


	public String getRestUserToken() {
		return restUserToken;
	}


	public void setRestUserToken(String restUserToken) {
		this.restUserToken = restUserToken;
	}

    
    

    
}